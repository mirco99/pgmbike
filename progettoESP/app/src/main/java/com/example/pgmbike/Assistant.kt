package com.example.pgmbike

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.pgmbike.activity.BubbleActivity
import com.example.pgmbike.database.MyOpenHelperDB

/**
    Classe per la gestione delle risposte da parte del bot
*/

object  Assistant
{

    private lateinit var myDB : SQLiteDatabase

    /********** funzione generale per il "riconoscimento" semplice del testo **********/
    /*
        Questa funzione lancia dei messaggi automatici da parte del bot/assistente
        per l'utente. Quest ultimo è in grado di conoscere quindi direttamente via
        chat il saldo residuo.
        Di default, se il bot non riconosce il testo del messaggio mandatom allora
        invia un messaggio di default con il comando disponibile per il credito.
     */
    fun recogniseFunction(message : MyMessage, context: Context)
    {
        val textContent = message.getText()
        if(textContent == "/saldo")
            getWallet(context) //--> lancio del messaggio contenente il saldo attuale
        else
            launchDF(context) //--> lancio del messaggio di default
    }

    /********** funzione per invio del credito residuo via chat **********/
    private fun getWallet(context: Context)
    {
        //apertura database e richiesta del saldo
        myDB = MyOpenHelperDB(context).writableDatabase
        val reqQuery = "SELECT * FROM " + MyOpenHelperDB.TABLE_USER
        val res = myDB.rawQuery(reqQuery, null).also {
            it.moveToFirst()
        }
        //ottenimento del saldo. Il messaggio da parte dell'assistente sarà relativo al credito
        val saldo = "${context.getString(R.string.balanceInfo)} ${
        res.getString(res.getColumnIndex(MyOpenHelperDB.SALDO))}$"
        //Genereazione dell'id e del messaggio
        val id = 2 * Chat.k
        Chat.k = id
        Chat.addMessage(MyMessage(id, saldo, System.currentTimeMillis()))
        //update dell'adapter se l'activity è attiva
        if(BubbleActivity.called) BubbleActivity.updateAdapter()
    }

    /********** funzione per invio di messaggio predefinito **********/
    private fun launchDF(context : Context)
    {
        //Genereazione dell'id e del messaggio
        val id = 2 * Chat.k
        Chat.k = id
        //messaggio di default dell'assistente
        val du : String = context.getString(R.string.assistantInfo)
        Chat.addMessage(MyMessage(id, du, System.currentTimeMillis()))
        //update dell'adapter se l'activity è attiva
        if(BubbleActivity.called ) BubbleActivity.updateAdapter()
    }
}