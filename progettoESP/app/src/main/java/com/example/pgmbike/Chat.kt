package com.example.pgmbike

/**
 * Oggetto che contiene la Chat ovvero tutti i messaggi inviati e ricevuti.
 */
object Chat
{
    var k : Long = 1
    private val messages = mutableListOf<MyMessage>()
    //Messaggio di benvenuto
    init {
        val id = 2 * k
        k = id
        val message = MyMessage(id, "Benvenuto, sono l'assistente di PGMBike!", System.currentTimeMillis())
        addMessage(message)
    }

    fun getMessageList() : MutableList<MyMessage>
    {
        return messages
    }
    fun addMessage(message : MyMessage)
    {
        messages.add(message)
    }
}