package com.example.pgmbike

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import java.util.*


class MessAdapter(private val messagesList: MutableList<MyMessage>) : ListAdapter<MyMessage, MessAdapter.MessViewHolder>(DIFFCALLBACK)
{
    class MessViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
         private var messageText : TextView = itemView.findViewById(R.id.message)
         private var timeText : TextView = itemView.findViewById(R.id.timestamp)

        /*messLayout: layout del messaggio. Permette di fissare il colore del messaggio e il layout
        vero e proprio di esso.
        */
         private var messLayout : ConstraintLayout = itemView.findViewById(R.id.messLayout) //layout del messaggio

        /*masterLayout: layout in cui è contenuto il layout del messaggio (chat item). Permette di
        * spostare il messaggio a sinistra o destra dello schermo a seconda dell'id.
        * Ciò permette all'utente di capire quali sono i messaggi del bot e quelli inviati.
        */
         private var masterLayout : ConstraintLayout = itemView.findViewById(R.id.masterLayout)

        //per spostare e cambiare colore del messaggio, è necessario effettuarlo programmaticamente
        private var masterSet = ConstraintSet()
        private var messSet   = ConstraintSet()
        init {
            masterSet.clone(masterLayout)
            messSet.clone(messLayout)
        }

        /********** binding dei dati e del layout **********/
        @SuppressLint("SetTextI18n") //aggiunto intenzionalmente per non sollevare il Lint per la concatenazione stringhe
        fun bind(content: MyMessage)
        {
            if(content.id % 2 != 0L) //id PARI -> il messaggio è in USCITA
            {
                messLayout.setBackgroundResource(R.drawable.message_out) //impostazione  dell'aspetto del messaggio in uscita
                //spostamento del messaggio a destra
                masterSet.clear(messLayout.id, ConstraintSet.LEFT) //eliminazione della dipendenza dal lato sinistro del layout master
                masterSet.connect(messLayout.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
                masterSet.connect(messLayout.id, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT)
                masterSet.applyTo(masterLayout)
            }
            else  //id DISPARI -> il messaggio è in ENTRATA
            {
                messLayout.setBackgroundResource(R.drawable.message_in) //imposto l'aspetto del messaggio in entrata
                //spostamento del messaggio a destra
                masterSet.clear(messLayout.id, ConstraintSet.RIGHT) //eliminazione della dipendenza dal lato destro del layout master
                masterSet.connect(messLayout.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
                masterSet.connect(messLayout.id, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT)
                //applicazione del layout
                masterSet.applyTo(masterLayout)
            }

            //visualizzazione del testo del messaggio
            messageText.text = content.getText()
            //visualizzazione dell'orario del messaggio
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = content.getTime()
            val minutes = calendar.get(Calendar.MINUTE)
            //stringa del tipo : hh:mm
            timeText.text = "${calendar.get(Calendar.HOUR_OF_DAY)}:${if (minutes > 9) minutes.toString() else "0$minutes"}"
        }
    }

    /********** CreateViewHolder **********/
    /*
        Alla creazione del "cassetto" della lista viene creata la view corrispondente (chat item).
    */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessViewHolder
    {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.chat_item, parent, false)
        return MessViewHolder(view)
    }

    /********** onBindViewHolder **********/
    /*
        permette di riempire gli elementi della lista con i dati corrispondenti.
    */
    override fun onBindViewHolder(holder: MessViewHolder, position: Int)
    {
        val content = messagesList[position]
        holder.bind(content)
    }

    /********** getItemCOunt **********/
    /*
        ottiene la dimensione della lista di elementi
    */
    override fun getItemCount(): Int
    {
        return messagesList.size
    }
}

/********** calcolo delle differenze tra liste di messaggi **********/
/*
    Permette l'aggiornamento della recycler view tramite la classe DiffUtil.
 */
private val DIFFCALLBACK =  object : DiffUtil.ItemCallback<MyMessage>()
{
    /*
        controllo se i due elementi contengono gli stessi dati. In questo caso è necessario
        esaminare che i due id dei messaggi siano gli stessi.
     */

    override fun areItemsTheSame(oldItem: MyMessage, newItem: MyMessage): Boolean {
        return oldItem.getID() == newItem.getID()
    }

    //controllo se i due elementi sono uguali
    override fun areContentsTheSame(oldItem: MyMessage, newItem: MyMessage): Boolean {
        return oldItem == newItem
    }
}
