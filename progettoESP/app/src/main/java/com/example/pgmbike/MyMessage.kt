package com.example.pgmbike

/**Oggetto Messaggio*/
data class MyMessage(val id : Long,
                     val textm : String,
                     val timestamp: Long)
{
    fun getText() : String
    {
        return textm
    }
    fun getTime() : Long
    {
        return timestamp
    }
    fun getID() : Long
    {
        return id
    }
}