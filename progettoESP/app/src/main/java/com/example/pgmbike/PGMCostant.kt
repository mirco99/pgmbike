package com.example.pgmbike

/**
 * Classe enumerativa di supporto, in cui sono presenti tutta una serie di costanti che vengono più
 * volte utilizzate in diversi codici sorgente
 */
enum class PGMCostant(val v:Int)
{
    GENERIC_CHANNEL(2340)   /*ID del canale per notifica generica*/,
    MESG_CHANNEL(4042)      /*ID canale per notifica messaggistica*/,
    MEDIA_CHANNEL(2043)     /*ID canale per i media*/,

    ID_GEN_NOT(30)      /*chiave per la recuperare una referenza di notifica generale*/,
    ID_MSG_NOT(40)      /*chiave per la recuperare una referenza di notifica messaggistica*/,
    ID_MEDIA_NOT(50)    /*chiave per la recuperare una referenza di notifica dei media*/,

    KEY_USER_REGISTERED(10)/*key per sapere se esiste oppure no un utente registrato*/,
    KEY_NAVIGATION_LAUNCHED(20) /*Key da usare con le SharedPreferences per sapere se è stata lanciata la NavigationActivity*/,

    JOB_ID(1)       /*id unico per il "lavoro" di servizio di notifica"*/,

    REPL_REQ(1)         /*codice identificativo del pending intent per il reply*/
}