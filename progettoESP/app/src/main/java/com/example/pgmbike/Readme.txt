L'applicazione mostra l'utilizzo delle nuove funzionalità di Android11 all'interno di un'app che gestisce
il noleggio di una bici.

Subito dopo l'installazione si avvia Activity0 che consente la registrazione dell'utente.
Una volta registrato si avvia Activity1 che è quella principale. Da qui infatti è possibile scegliere
che bici noleggiare tramite un codice, un numero multipo di 5 da 5 a 300 va bene.
Una volta scelto il codice e ricaricato il portafoglio è possibile iniziare la navigazione.
L'avvio di NavigationActivity avviene con la pressione dello start, da qui è possibile avviare un timer con il
conseguente addebito sul portafoglio.

Tutti questi passaggi sono accompagnati dall'apparizione di diverse notifiche.
In particolare esiste una BOT, che compare all'interno della Bubble, con il quale l'utente interagisce.
Il BOT infatti invia diversi messaggi. E' possibile richiedere il saldo disponibile, inviando il
comando: /saldo.
Inoltre nel momento in cui il portafoglio si svuota il BOT segnalerà il fatto con un messaggio, chiudendo
l'activity e ritornando ad Activity1.

Dalla NavigationActiviy è inoltre possibile avviare un brano musicale che verrà segnalato tramite una
notifica di tipo Media.