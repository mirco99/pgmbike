package com.example.pgmbike

import android.app.RemoteInput
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.pgmbike.activity.BubbleActivity
import com.example.pgmbike.notifiche.MessageNotificationBuilder
import com.example.pgmbike.notifiche.PGMNotificationManager

/**
    Classe per la gestione dei messaggi inviati da remoto direttamente dalla notifica di messaggistica.
    L'utente è quindi in grado interagire con l'assistente direttamente dal pannello delle notifiche,
    in particolare tramite il pulsante di "reply" creato nella classe di gestione delle notifiche di
    messaggistica.
    Ciò che permette di svolgere questa classe è di raccogliere l'intent remoto della notifica a cui è
    associato il messaggio di testo inviato dall'utente e di aggiornare la notifica stessa per
    visualizzare sia i messaggi inviati che quelli ricevuti mediante questo modo.
 */
class ReplyReceiver : BroadcastReceiver()
{
    companion object {
        const val KEY_TEXT_REPLY = "reply" //permette di capire il broadcast giusto inviato dalla notifica
    }
    /********** funzione per la gestione della ricezione **********/
    /*
        La funzione permette di raccogliere il broadcast ed estrarre la stringa di testo che
        l'utente ha immesso e inviato direttamente dalla notifica.
        Una volta ottenuta la stringa, si crea un nuovo messaggio dell'utente, lo si inserisce nella
        lista di messaggi e si aggiorna il testo della notifica.
        La stringa viene anche riconosciuta dal bot e invia anch'esso la risposta.
        L'interazione avviene quindi interamente da notifica.
     */
    override fun onReceive(context: Context?, intent: Intent?)
    {
        //estrazione della stringa ricevuta dall'intent di notifica
        val results = RemoteInput.getResultsFromIntent(intent) ?: return
        val input = results.getCharSequence(KEY_TEXT_REPLY)?.toString() //testo inviato dall'utente

        //Generazione del messaggio
        val id = 2 * Chat.k + 1
        Chat.k = id
        val myMess = MyMessage(id, input.toString(), System.currentTimeMillis())
        Chat.addMessage(myMess)
        //recupero della referenza della notifica di messaggio
        var msgNot : MessageNotificationBuilder
        try
        {
            val mNNullable = PGMNotificationManager.getRef(PGMCostant.ID_MSG_NOT.v) as MessageNotificationBuilder?
            if(mNNullable!=null) msgNot=mNNullable
            else throw NullPointerException()
        }
        catch (e: NullPointerException)
        {
            //Se non è presente la notifica nel registro delle notifiche la si crea da zero
            msgNot= context?.let { buildMsgNot(it) }!!
        }

        /* L'aggiornamento della notifica avviene se e solo se si riceve del testo
           del contesto validi
         */
        if (input != null && context != null) {
            updateNotification(msgNot, input, context)
        }

        if (context != null) {
            //riconoscimento della stringa estratta
            Assistant.recogniseFunction(myMess, context)
            val lastMess = Chat.getMessageList().last()
            if(lastMess.id % 2L == 0L) //messaggio dal bot
                 updateNotification(msgNot, lastMess.getText() , context)
        }

    }

    /********** costruzione notifica messaggistica (se riferimento non presente)  **********/
    private fun buildMsgNot(context : Context):MessageNotificationBuilder
    {
        //NOTIFICA MESSAGGISTICA
        val mN= MessageNotificationBuilder(
                context,
                PGMCostant.MESG_CHANNEL.v,
                context.getString(R.string.MSG_CH_NAME),
                context.getString(R.string.MSG_CH_DESC)
        )
        val myIntent = Intent(context, BubbleActivity::class.java)
                .setAction(Intent.ACTION_VIEW)
                .apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TASK }
        mN.setIntent(myIntent)
        PGMNotificationManager.register(PGMCostant.ID_MSG_NOT.v,mN) //Registro la referenza di quel costruttore

        return mN
    }

    /********** funzione per l'aggiornamento della notifica **********/
    private fun updateNotification(notification : MessageNotificationBuilder, input : String, context: Context) {
        notification.setTitleandText(context.getString(R.string.assistant_name), input)
        PGMNotificationManager.setBuilder(notification)
        PGMNotificationManager.show(context)
    }

}