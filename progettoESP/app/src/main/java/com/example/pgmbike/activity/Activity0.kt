package com.example.pgmbike.activity

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.icu.util.Calendar
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.pgmbike.PGMCostant
import com.example.pgmbike.R
import com.example.pgmbike.database.MyOpenHelperDB
import com.example.pgmbike.notifiche.GenericNotificationBuilder
import com.example.pgmbike.notifiche.PGMNotificationManager
import com.google.android.material.snackbar.Snackbar
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Activity che gestisce il login dell'utente, richiede di impostare varie credenziali.
 * Viene chiamata solo la prima volta che l'utente usa l'app; non appena registra le sue credenziali,
 * si passa ad Activity1. Solo nel caso in cui venga cancellata la memoria dell'app nelle impostazioni, o
 * se l'app viene disinstallata, si tornerà di nuovo in Activity0.
 */

class Activity0 : AppCompatActivity() {
    private lateinit var username: EditText        //Nome utente
    private lateinit var usersurname: EditText     //Cognome utente
    private lateinit var email: EditText           //Email
    private lateinit var password: EditText        //Password
    private lateinit var confirmPassword: EditText //Conferma password
    private lateinit var dateOfBirth: EditText     //Data di compleanno
    private lateinit var button : Button           //Bottone di conferma
    private lateinit var calendar : Button         //Bottone per il calendario

    //SharedPreferences
    private val pgmPref="mypreference"

    companion object
    {
        private const val USER_REGISTERED="UserRegistered"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Display the layout
        setContentView(R.layout.activity_0)
        button = findViewById(R.id.bu)

        //get references to widget
        username = findViewById(R.id.username)
        usersurname = findViewById(R.id.usersurname)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        confirmPassword = findViewById(R.id.confirm_password)
        dateOfBirth = findViewById(R.id.date_of_birth)
        calendar = findViewById(R.id.calendar)


        //TextWatcher per gestire i cambiamenti fatti da tastiera sugli editText
        class MyTextWatcher constructor(private val view: View) : TextWatcher
        {

            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                //verifico subito dopo l'immissione di un carattere se questo realizza una sequenza valida per i seguenti campi
                when (view.id) {
                    R.id.email -> isValidEmail((email.text).toString())
                    R.id.password -> isValidPassword((password.text).toString())
                    R.id.confirm_password -> isConfirmedPassword()
                    R.id.date_of_birth -> isValidDate((dateOfBirth.text).toString())
                    else -> { }
                }
            }
        }
        //Listener per i TextWatcher
        username.addTextChangedListener( MyTextWatcher(username))
        usersurname.addTextChangedListener( MyTextWatcher(usersurname))
        email.addTextChangedListener( MyTextWatcher(email))
        password.addTextChangedListener( MyTextWatcher(password))
        confirmPassword.addTextChangedListener( MyTextWatcher(confirmPassword))
        dateOfBirth.addTextChangedListener( MyTextWatcher(dateOfBirth))

        //Gestione del bottone calendario
        //Quando l'utente preme nel bottone, si apre un calendario con il quale è possibile scegliere una
        //data, che verrà poi inserita nell'EditText dateOfBirth
        calendar.setOnClickListener {
            val myCalendar: Calendar = Calendar.getInstance()
            val date =                                                  // in date salvo la data scelta nel calendario
                OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    myCalendar.set(Calendar.YEAR, year)
                    myCalendar.set(Calendar.MONTH, monthOfYear)
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    val myFormat = "dd/MM/yyyy" // formato data
                    val sdf = SimpleDateFormat(myFormat, Locale.getDefault() ) // imposto la data con il formato scelto
                    dateOfBirth.setText(sdf.format(myCalendar.time))   //aggiorno l'EditText dateOfBirth
                }

            // mostro il calendario, mettendo come data di default 01/01/1999
            DatePickerDialog(
                this,
                date,
                1999,
                0,
                1
            ).show()
        }

        //Gestione del bottone di conclusione dell'activity per passare ad Activity1 se tutti i campi sono validi
        button.setOnClickListener{
            if( allFieldsCompiled() &&
                isValidEmail(email.text.toString())
                && isValidPassword((password.text).toString()) &&
                isConfirmedPassword() &&
                isValidDate((dateOfBirth.text).toString()))
            {

                //Se si entra qui implica che tutti i campi sono corretti ed è possibile salvare
                //i dati nel database e chiudere l'activity

                //Salvo nel database i dati
                val myDB= MyOpenHelperDB(this).writableDatabase  //Creo o apro il databese

                //Registro l'utente nel DB, saldo di default a 0
                updateUtente(
                        username.text.toString(),
                        usersurname.text.toString(),
                        0,
                        password.text.toString(),
                        email.text.toString(),
                        dateOfBirth.text.toString(),
                        "ALL",/*Aggiorno tutti i campi*/
                        myDB)

                //Salvo nelle preferences che si è registrato un utente valido
                val preferences = getSharedPreferences(pgmPref, Context.MODE_PRIVATE)
                val editor = preferences.edit()
                editor.putString(PGMCostant.KEY_USER_REGISTERED.v.toString(), USER_REGISTERED)
                editor.apply()

                //Notifica di benvenuto
                //Riprendo la referenza a ID_MSG_NOT,
                //se la referenza non è presente, è lanciata un'eccezione, che viene catturata lanciando la notifica
                var gN: GenericNotificationBuilder
                try
                {
                    val gNNullable = PGMNotificationManager.getRef(PGMCostant.ID_GEN_NOT.v) as GenericNotificationBuilder?
                    if(gNNullable != null)
                        gN = gNNullable
                    else throw NullPointerException()
                }
                catch (e: NullPointerException)
                {
                    //creo la notifica col costruttore
                    gN = GenericNotificationBuilder(
                        this,
                        PGMCostant.GENERIC_CHANNEL.v,
                        getString(R.string.GENERIC_CH_NAME),
                        getString(R.string.GENERIC_CH_DESC)
                    )

                    //registro la referenza
                    PGMNotificationManager.register(PGMCostant.ID_GEN_NOT.v,gN)
                }

                gN.setTitleandText("Benvenuto", "Ciao ${username.text} buon divertimento")
                gN.setIntent(Intent(this,Activity1::class.java))
                PGMNotificationManager.setBuilder(gN)
                PGMNotificationManager.show(this)

                //Avvio l'Activity1
                val goToActivity1 = Intent(this, Activity1::class.java)
                goToActivity1.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                startActivity(goToActivity1)
                finish()
            }

            //se uno o più campi non sono validi, appare una snackbar che allerta l'utente sul relativo errore
            else if(!allFieldsCompiled())
                Snackbar.make(it, "Imposta tutti i campi correttamente", Snackbar.LENGTH_SHORT).show()

            else if(allFieldsCompiled() && !isValidEmail(email.text.toString()))
                Snackbar.make(it, "Indirizzo email non valido", Snackbar.LENGTH_SHORT).show()

            else if(allFieldsCompiled() && !isValidPassword(password.text.toString()))
                Snackbar.make(it, "Password non valida", Snackbar.LENGTH_SHORT).show()

            else if(allFieldsCompiled() && isValidPassword(password.text.toString()) && !isConfirmedPassword())
                Snackbar.make(it, "Le due password non combaciano", Snackbar.LENGTH_SHORT).show()

            else if(allFieldsCompiled() && !isValidDate(dateOfBirth.text.toString()))
                Snackbar.make(it, "Formato data non corretto", Snackbar.LENGTH_SHORT).show()
        }

    }

    //Aggiornamento del database, in base al TAG è possibile aggiornare solo un campo oppure tutti
    private fun updateUtente(nome:String="noName", cognome:String="noSurname", saldo:Int,
                             psw:String, email: String, birth:String,
                             tag:String="ALL",DB:SQLiteDatabase)
    {
        val tmp= ContentValues()
        when(tag)
        {
            "SALDO"->tmp.put(MyOpenHelperDB.SALDO, saldo.toString())
            else->
            {
                tmp.put(MyOpenHelperDB.NOME,nome)
                tmp.put(MyOpenHelperDB.COGNOME,cognome)
                tmp.put(MyOpenHelperDB.SALDO, saldo.toString())
                tmp.put(MyOpenHelperDB.PSW,psw)
                tmp.put(MyOpenHelperDB.EMAIL,email)
                tmp.put(MyOpenHelperDB.BIRTH,birth)
            }
        }

        DB.update(MyOpenHelperDB.TABLE_USER,tmp,null,null) //Update dell'utente
        DB.close()
    }


    private fun isValidDate(date : String) : Boolean
    {
        return try {
            val df: DateFormat = SimpleDateFormat("dd/MM/yyyy")
            df.isLenient = false
            df.parse(date) //analizzo formato di data che ho impostato con quello della data passata come stringa
            true
        } catch (e: ParseException) {
            false
        }
    }

    private fun isConfirmedPassword() : Boolean
    {
        return password.text.toString() == (confirmPassword.text).toString()
    }

    //si verifica e si allerta l'utente se la password che sino a quel momento si è inserito è valida
    //si mostra un messaggio con i vincoli da rispettare per la password
    private fun isValidPassword(pass : String) : Boolean
    {
        pass.let {
            val passwordPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$" //pattern con caratteri maiuscoli, minuscoli, numeri, caratteri speciali e di 8 cifre, (?=\S+$) non consente spazi vuoti
            val passwordMatcher = Regex(passwordPattern) //Regex mi dà la possibilità di operare con vari metodi sul pattern..come find(), contains() etc..

            if (passwordMatcher.find(pass) == null){ // find() ritorna il primo elemento che combacia con l'input pass, o null altrimenti
                password.error = getString(R.string.err_msg_password)
                requestFocus(password)
                return false
            }
            else {
                password.error = null
            }
            return true
        }

    }

    private fun isValidEmail(mail : String) : Boolean
    {
        if (TextUtils.isEmpty(mail) || !android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
            email.error = getString(R.string.err_msg_email)
            requestFocus(email)
            return false
        }
        else {
            email.error = null
        }
        return true
    }

    //quando clicco il bottone e ho la richiesta di focus, torno nel campo focussato..ad esempio se email è sbagliata e provo
    //a premere il bottone mi fa tornare sul campo email
    private fun requestFocus(view : View)
    {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }
    }

    //verifico che i campi non sono vuoti
    private fun allFieldsCompiled() : Boolean
    {
        return  username.text.toString().isNotEmpty() &&
                usersurname.text.toString().isNotEmpty() &&
                email.text.toString().isNotEmpty() && password.text.toString().isNotEmpty() &&
                confirmPassword.text.toString().isNotEmpty() &&
                dateOfBirth.text.toString().isNotEmpty()

    }

}

