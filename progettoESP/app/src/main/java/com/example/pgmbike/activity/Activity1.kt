package com.example.pgmbike.activity

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import android.graphics.Color
import android.provider.Settings
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.core.text.isDigitsOnly
import androidx.core.view.isVisible
import com.example.pgmbike.*
import com.example.pgmbike.database.MyOpenHelperDB
import com.example.pgmbike.notifiche.GenericNotificationBuilder
import com.example.pgmbike.notifiche.MediaNotificationBuilder
import com.example.pgmbike.notifiche.MessageNotificationBuilder
import com.example.pgmbike.notifiche.PGMNotificationManager
import com.example.pgmbike.service.TimerNotificationService
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import java.lang.NullPointerException

/**
 * Activity PRINCIPALE che coordina Activity0 e NavigationActivity, da qui infatti è possibile attivare una delle due in base
 * allo stato dell'app.
 * E' possibile decidere di aprire Activity0 se nessun utente si è registrato.
 * Inoltre Activity1, gestisce i codici delle biciclette e il portafoglio, che permettono all'utente di viaggiare, ovvero
 * di aprire NavigationActivity
 */
class Activity1 : AppCompatActivity() {
    //DATABASE PER LE BICI
    private lateinit var myDB:SQLiteDatabase

    //BICI
    var biciUtente="0"      //conterrà il codice inserito dall'utente

    //UTENTE
    private var user:String="NomeUtenteNoR"             //Nome utente
    private var cognomeUser:String="Cognome Utente"     //Cognome utente
    private var saldo  : Short= 0                        //Saldo dell'utente
    private var email:String=""                         //email
    private var birth:String=""                         //data di nascita

    //NotificationManager
    private val myManager= PGMNotificationManager       //Referenza al PGMNotificationManager

    //SharedPreferences
    private val pgmPref="mypreference"
    private lateinit var sharedPreferences:SharedPreferences

    //View
    private lateinit var startButton : FloatingActionButton
    private lateinit var swAdd5 : SwitchCompat


    companion object
    {
        private const val NO_USER="noUser"
        //Chiavi di recupero dati
        private const val KEY_CODICE_BICI_UTENTE="CodiceBiciUtente"   //Chiave per recuperare il codice della bici salvato nelle preferences
        private const val KEY_PULSANTE_START="PulsanteStart"          //Chiave per recuperare lo stato del pulsante di avvio
        private const val KEY_SWITCH="SwitchRicarica"                 //Chiave per recuperare lo stato dello switch di ricarica
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1)

        //Costruzione delle notifiche
        buildNot()

        //Recupero del codice della bici già inserito, se presente
        val tmp=savedInstanceState?.getString(KEY_CODICE_BICI_UTENTE)
        if(tmp!=null) biciUtente=tmp

    }

    override fun onStart()
    {
        super.onStart()

        sharedPreferences = getSharedPreferences(pgmPref, Context.MODE_PRIVATE)

        /**
         * Nelle Shared Preferences viene salvato un flag che segnala se qualcuno si è già registrato,
         * se falso si attiva Activity0, ovvero l'activity di registrazione
         */
        if(sharedPreferences.getString(PGMCostant.KEY_USER_REGISTERED.v.toString(), NO_USER).equals(NO_USER))
        {
            val loginIntent = Intent(this, Activity0::class.java)
            loginIntent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY //L'activity viene rimossa non appena l'utete ha finito di registrarsi
            startActivity(loginIntent)
            finish()
        }

        /**
         * Nelle Shared Preferences viene salvato un flag che segnala se NavigationActivity era avviata
         * se vero la si riavvia
         */
        if(sharedPreferences.getBoolean(PGMCostant.KEY_NAVIGATION_LAUNCHED.v.toString(), false))
        {
            //In questo caso NavigationActivity è gia lanciata ed è in esecuzione quindi la lancio e termino Activity1
            //start di navigation activity
            val navIntent = Intent(this, NavigationActivity::class.java)
            startActivity(navIntent)
            finish()
        }

        //Recupero dello stato del pulsante di start
        startButton=findViewById(R.id.Start)
        if(sharedPreferences.getBoolean(KEY_PULSANTE_START, false)) startButton.isVisible=true

        //Recupero stato switch
        val swAdd5=findViewById<SwitchCompat>(R.id.swAdd5)              //Switch per incrementare di 5$
        if(sharedPreferences.getBoolean(KEY_SWITCH,false))swAdd5.isChecked=true
    }

    @SuppressLint("SetTextI18n")
    override fun onResume()
    {
        super.onResume()

        /*WIDGET NELLA UI */
        val tvNomeUtente=findViewById<TextView>(R.id.tvNomeUtente)
        val portafoglio=findViewById<TextView>(R.id.Saldo)              //TV per visualizzare il saldo

        val codiceBici = findViewById<EditText>(R.id.bikeCodeUI)        //EditText per inserire il codice delle bici

        val conf=findViewById<Button>(R.id.Conf)
        val ricarica=findViewById<Button>(R.id.Ricarica)                //Buttoon per ricaricare il portafogli

        swAdd5=findViewById(R.id.swAdd5)                                //Switch per incrementare di 5$

        startButton=findViewById(R.id.Start)                            //Bottone per far partire la corsa

        val imp=findViewById<FloatingActionButton>(R.id.Imp)            //Bottone per aprire le impostazioni
        val dataUser=findViewById<FloatingActionButton>(R.id.Utente)    //Bottone per visualizzare i dati dell'utente

        //TextWatcher per l'inserimento del codice bici
        val codWatcher = object : TextWatcher
        {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                conf.text=getString(R.string.CONF_BICI)
                conf.setBackgroundColor(Color.BLUE)  //Cambio colore del bottone in BLUE
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                biciUtente = s.toString()
                startButton.isVisible=false
            }

            override fun afterTextChanged(s: Editable?) {
                // This one too
            }
        }
        //LISTENER per il TextWatcher
        codiceBici.addTextChangedListener(codWatcher)

        //RECUPERO INFO UTENTE DAL DATABASE
        /**
         * DATABASE BICI
         * Si crea o apre un database che contiene i codici delle bici
         */
        myDB= MyOpenHelperDB(this).writableDatabase

        //Recupero le informazioni del saldo
        if(recuperoSaldo())
        {
            portafoglio.text= "$saldo $"
        }
        else portafoglio.text=getString(R.string.NO_VAL)

        //Recupero le informazioni sull'utente in un thread separato
        Thread{
            myDB= MyOpenHelperDB(this).writableDatabase
            //Recupero le informazioni del saldo
            if(recuperoSaldo()) portafoglio.post { portafoglio.text = "$saldo $" }
            else portafoglio.post { portafoglio.text = getString(R.string.NO_VAL) }
            user=recuperoNome()
            tvNomeUtente.post { tvNomeUtente.text = user }
            cognomeUser=recuperoCognome()
            email=recuperoEmail()
            birth=recuperoDataNascita()
        }.start()

        /**
         * Listener dei vari pulsanti
         */

        //Pulsate per la conferma della bici
        conf.setOnClickListener{
            //Query per la ricerca del codice bici
            val sql = "select " + MyOpenHelperDB.COD + " from " + MyOpenHelperDB.TABLE + " where " + MyOpenHelperDB.COD +
                    " like "+ "'$biciUtente'"
            val risQuery: Cursor    //Risultato della query
            risQuery=myDB.rawQuery(sql,null)

            if(risQuery.count==1)
            {
                //Se si entra qui significa che nel database c'è una voce con quel codice
                if(saldo > TimerNotificationService.pull)
                {
                    startButton.isVisible=true
                    conf.text=getString(R.string.BICI_VALIDA)
                    conf.setBackgroundColor(Color.GREEN)    //Bottone di colore VERDE
                    val imm: InputMethodManager =           //La tastiera viene nascosta per visualizzare il bottone
                        getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(window.decorView.rootView.windowToken,0)
                }
                else
                {
                    val imm: InputMethodManager =
                        getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(window.decorView.rootView.windowToken,0)

                    //Se l'utente ha inserito il codice della bici corretto ma non ha abbastanza soldi
                    //compare una SnackBar che lo informa di questo

                    Snackbar.make(it, getString(R.string.NO_MONEY), Snackbar.LENGTH_SHORT).show()
                    conf.text=getString(R.string.CONF_BICI)
                    conf.setBackgroundColor(Color.BLUE)  //Bottone BLU
                }
            }
            else
            {
                //Se si entra qui vuol dire che nel database non c'è una voce con quel codice
                conf.text=getString(R.string.BICI_NON_VALIDA)
                conf.setBackgroundColor(Color.RED)  //Colore del bottone ROSSO
                startButton.isVisible=false

            }
            risQuery.close()
        }

        //Bottone per la ricarica del portafoglio
        ricarica.setOnClickListener {
            if(swAdd5.isChecked) saldo = (saldo + 5).toShort()
            else saldo++
            portafoglio.text= "$saldo $"
        }

        //Pulsante per avviare l'activity di navigazione
        startButton.setOnClickListener{
            //Si aggiunge un messaggio alla BubbleActivity
            addMessageToBubble("Buon Viaggio $user")

            //Recupero la notifica messaggistica
            val mN:MessageNotificationBuilder = try {
                val mNNullable = PGMNotificationManager.getRef(PGMCostant.ID_MSG_NOT.v) as MessageNotificationBuilder?
                if(mNNullable!=null) mNNullable
                else throw NullPointerException()
            } catch (e:NullPointerException) {
                //Se non è presente la notifica nel registro delle notifiche la si crea da zero
                buildMsgNot()
            }

            //aggiornamento dell'adapter, se la Bubbleactivity è attiva
            if(BubbleActivity.called) BubbleActivity.updateAdapter()

            mN.setTitleandText("PARTENZA","Buon Viaggio $user")
            myManager.setBuilder(mN)
            myManager.show(this)

            //Si salva nelle sharedPreferences che NavigationActivity è stata lanciata
            val edit=sharedPreferences.edit()
            edit.putBoolean(PGMCostant.KEY_NAVIGATION_LAUNCHED.v.toString(),true)
            startButton.isVisible=false
            edit.putBoolean(KEY_PULSANTE_START,startButton.isVisible)   //Si nasconde il pulsante di start
            edit.apply()

            //start di navigation activity
            val navIntent = Intent(this, NavigationActivity::class.java)
            startActivity(navIntent)
        }

        //Bottone per accedere alle impostazioni delle notifiche dell'applicazione
        imp.setOnClickListener {
            val intent = Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS).apply {
                putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
            }
            startActivity(intent)
        }

        //Bottone per la richiesta dei dati dell'utente
        dataUser.setOnClickListener{
            //Si aggiunge un messaggio nella BubbleActivity dove compaiono i dati dell'utente
            addMessageToBubble(builDatiUtente())
            //Recupero la notifica messaggistica
            var mN:MessageNotificationBuilder = try
            {
                val mNNullable = PGMNotificationManager.getRef(PGMCostant.ID_MSG_NOT.v) as MessageNotificationBuilder?
                if(mNNullable!=null) mNNullable
                else throw NullPointerException()
            } catch (e:NullPointerException) {
                //Se non è presente la notifica nel registro delle notifiche la si crea da zero
                buildMsgNot()
            }
            mN.setTitleandText("DATI UTENTE","Ecco i dati richiesti")
            myManager.setBuilder(mN)
            myManager.show(this)
        }
    }

    override fun onPause()
    {
        super.onPause()
        //Salvataggio dei   dati
        updateUtente(user,cognomeUser,saldo,"SALDO")
        //Chiusura del database
        myDB.close()

        //Salvataggio nelle SharedPreferences dello stato del pulsante di start e dello switch
        val edit=sharedPreferences.edit()
        edit.putBoolean(KEY_PULSANTE_START,startButton.isVisible)
        edit.putBoolean(KEY_SWITCH,swAdd5.isChecked)
        edit.apply()
    }

    /**
     * Salvataggio del codice bici inserito dall'utente
     */
    override fun onSaveInstanceState(outState: Bundle)
    {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_CODICE_BICI_UTENTE,biciUtente)
    }

    /**
     * Si aggiunge un messaggio che verrà visualizzato nella Bubble
     */
    private fun addMessageToBubble(msg:String)
    {
        //Recupero la lista dei messaggi della chat e ne inserisco uno
        val newMessage = MyMessage(12,msg, System.currentTimeMillis())
        Chat.addMessage(newMessage)
        return
    }

    /**
     * Costruzione di una stringa che contine i dati dell'utente
     */
    private fun builDatiUtente():String
    {
        return "Dati:\n${user}"+" ${cognomeUser}\n"+"email: ${email}\n"+"Data di nascita: $birth"
    }

    /**
     *Recupero del saldo dal database, se falso c'è stato qualche problema
     */
    private fun recuperoSaldo():Boolean
    {
        val sqlQuery="select * from "+ MyOpenHelperDB.TABLE_USER //Seleziono tutte le righe di TABLE_USER
        val res=myDB.rawQuery(sqlQuery,null)  //Eseguo la query
        res.moveToFirst()
        val saldoDB=res.getString(res.getColumnIndex(MyOpenHelperDB.SALDO)) //Cerco il saldo dal database
        res.close() //Chiudo Cursor

        if(saldoDB.isDigitsOnly())
        {
            saldo= saldoDB.toShort()       //Aggiorno la variabile saldo
            return true
        }
        //Se viene ritornato false c'è stato qualche problema con il recupero del saldo
        return false
    }

    /**
     * Recupero del nome dell'utente
     */
    private fun recuperoNome():String
    {
        val sqlQuery="select * from "+ MyOpenHelperDB.TABLE_USER    //Seleziono tutte le righe di TABLE_USER
        val res=myDB.rawQuery(sqlQuery,null)            //Eseguo la query
        res.moveToFirst()
        val nome=res.getString(res.getColumnIndex(MyOpenHelperDB.NOME))    //nome dell'utente e chiusura del Cursor
        res.close()
        return nome
    }

    /**
     * Recupero del cognome dell'utente
     */
    private fun recuperoCognome():String
    {
        val sqlQuery="select * from "+ MyOpenHelperDB.TABLE_USER //Seleziono tutte le righe di TABLE_USER
        val res=myDB.rawQuery(sqlQuery,null)        //Eseguo la query
        res.moveToFirst()
        val cognome=res.getString(res.getColumnIndex(MyOpenHelperDB.COGNOME))    //cognome dell'utente e chiusura del Cursor
        res.close()
        return cognome
    }

    /**
     * Recupero dell'email dell'utente
     */
    private fun recuperoEmail():String
    {
        val sqlQuery="select * from "+ MyOpenHelperDB.TABLE_USER    //Seleziono tutte le righe di TABLE_USER
        val res=myDB.rawQuery(sqlQuery,null)            //Eseguo la query
        res.moveToFirst()
        val emailUtente=res.getString(res.getColumnIndex(MyOpenHelperDB.EMAIL))    //email dell'utente e chiusura del Cursor
        res.close()
        return emailUtente
    }

    /**
     * Recupero della data di nascita
     */
    private fun recuperoDataNascita():String
    {
        val sqlQuery="select * from "+ MyOpenHelperDB.TABLE_USER //Seleziono tutte le righe di TABLE_USER
        val res=myDB.rawQuery(sqlQuery,null)        //Eseguo la query
        res.moveToFirst()
        val data=res.getString(res.getColumnIndex(MyOpenHelperDB.BIRTH))    //Data di nascita dell'utente e chiusura del Cursor
        res.close()
        return data
    }

    /**
     * Salvataggio dei dati nel database
     * Se il tag è: ALL Aggiorna tutti i campi
     * Se è: SALDO, aggiorna solo il saldo
     */
    private fun updateUtente(nome:String="noName", cognome:String="noSurname", saldo: Short, tag:String="ALL")
    {
        val tmp=ContentValues()
        /**
         * A seconda del valore del tag vengono salvate/aggiornata più o meno informazioni
         */
        when(tag){
            "SALDO"->tmp.put(MyOpenHelperDB.SALDO, saldo.toString())
            else->
            {
                tmp.put(MyOpenHelperDB.NOME,nome)
                tmp.put(MyOpenHelperDB.COGNOME,cognome)
                tmp.put(MyOpenHelperDB.SALDO, saldo.toString())
            }
        }
        myDB.update(MyOpenHelperDB.TABLE_USER,tmp,null,null) //Update dell'utente
    }

    /**
     * Costruzione di 3 tipi di notifiche con i rispettivi canali e
     * registrazione di queste al PGMNotificationManager
     */
    private fun buildNot()
    {
        buildGenericNot()
        buildMsgNot()
        buildMediaNot()
    }

    /**
     * Costruzione notifica per la gestione dei media
     */
    private fun buildMediaNot():MediaNotificationBuilder
    {
        //NOTIFICA PER I MEDIA
        val mediaNot = MediaNotificationBuilder(
                this,
                PGMCostant.MEDIA_CHANNEL.v,
                getString(R.string.MEDIA_CH_NAME),
                getString(R.string.MEDIA_CH_DESC)
        )
        //Registro la referenza di quel costruttore
        PGMNotificationManager.register(PGMCostant.ID_MEDIA_NOT.v,mediaNot)
        return mediaNot
    }

    /**
     * Costruzione notifica per la gestione dei messaggi con bubble
     */
    private fun buildMsgNot():MessageNotificationBuilder
    {
        //NOTIFICA MESSAGGISTICA
        val mN= MessageNotificationBuilder(
                this,
                PGMCostant.MESG_CHANNEL.v,
                getString(R.string.MSG_CH_NAME),
                getString(R.string.MSG_CH_DESC)
        )
        val myIntent = Intent(this, BubbleActivity::class.java)
                .setAction(Intent.ACTION_VIEW)
                .apply {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
        mN.setIntent(myIntent)
        PGMNotificationManager.register(PGMCostant.ID_MSG_NOT.v,mN) //Registro la referenza di quel costruttore
        //******************

        return mN
    }

    /**
     * Costruzione notifica di uso generale
     */
    private fun buildGenericNot():GenericNotificationBuilder
    {
        //NOTIFICA GENERICA
        //Costruttore per la notifica
        val gN= GenericNotificationBuilder(
                this,
                PGMCostant.GENERIC_CHANNEL.v,
                getString(R.string.GENERIC_CH_NAME),
                getString(R.string.GENERIC_CH_DESC)
        )

        PGMNotificationManager.register(PGMCostant.ID_GEN_NOT.v,gN) //Registro la referenza di quel costruttore
        //******************
        return gN
    }
}