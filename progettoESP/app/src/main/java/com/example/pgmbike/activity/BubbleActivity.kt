package com.example.pgmbike.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView
import com.example.pgmbike.*
import java.lang.IndexOutOfBoundsException

/**
 * Activity di messaggistica che verrà visualizzata all'interno della Bubble
 */
class BubbleActivity : AppCompatActivity()
{
    private lateinit var send : ImageButton //pulsante di invio messaggio
    private var text : String = "" //testo del messaggio

    init {
        called = false //l'activity di default non è attiva
    }
    companion object
    {
        var called = false
        private var messRecyclerView : RecyclerView? = null
        private var myAdapter : MessAdapter? = null

        /********** funzione per l'aggiornamento dell'adapter dall'esterno **********/
        /*
            Permette di aggiornare l'adapter e quindi la sua view associata da altre activity
            o service o broadcast receiver.
            L'update avviene solo quando l'activity corrente è attiva in modo da poter visualizzare
            i nuovi messaggi in arrivo in tempo reale.
         */
        fun updateAdapter()
        {
            //ottenimento di una lista di messaggi senza riferimento a quella originale
            val messagesU : MutableList<MyMessage> =  mutableListOf()
            getMessageWithoutRif(messagesU)
            //aggiornamento dell'adapter
            try {
                myAdapter?.submitList(messagesU)
                /*spostamento della scrollbar della recyclerView in modo tale da visualizzare sempre
                 * l'ultimo messaggio della chat */
                myAdapter?.itemCount?.let { n -> messRecyclerView?.smoothScrollToPosition(n) }
            }
            catch (e : IndexOutOfBoundsException) {
                //non si aggiunge l'ultimo messaggio ma si mostra solo l'ultimo
                myAdapter?.itemCount?.let { n -> messRecyclerView?.smoothScrollToPosition(n) }
                Log.e("exception", "verified")
            }
        }

        /********** metodo per ottenere una lista di messaggi senza riferimenti  **********/
        private fun getMessageWithoutRif(messages : MutableList<MyMessage>)
        {
            for(item : MyMessage in Chat.getMessageList()) messages.add(item.copy())
        }
    }


    override fun onCreate(savedInstanceState: Bundle?)
    {
        called = true //ora l'activity corrente è attiva
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bubble)

        //inflate della recycler view dei messaggi
        messRecyclerView  = findViewById(R.id.messRecyclerView)

        //ottengo la lista dei messaggi della chat
        myAdapter = MessAdapter(Chat.getMessageList())
        messRecyclerView?.adapter = myAdapter

        //dichiarazione dell'edit text e del pulsante di invio
        val inputText : EditText = findViewById(R.id.inputText)
        send = findViewById(R.id.send)


        //quando si preme per immettere del testo, quello di default viene cancellato
        inputText.setOnFocusChangeListener{ _: View, _: Boolean ->
            inputText.setText(text)
        }

        /********** watcher per l'edit text  **********/
        val codeWatcher = object : TextWatcher
        {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int)
            {
                text = ""
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
            {
                text = s.toString()
            }

            override fun afterTextChanged(s: Editable?)
            {

            }
        }
        inputText.addTextChangedListener(codeWatcher)

        /********** listener per il pulsante di invio **********/
        /*
            Quando premuto "invio", viene creato un messaggio da parte dell'utente e immesso nella
            lista dedicata per i messaggi.
            Dopodichè si aggiorna l'adapter per visualizzare in chat il messaggio dell'utente.
            Si fa partire dunque la funzione di riconoscimento del messaggio inviato da parte dell'
            utente.
         */
        send.setOnClickListener{
            if(text == "")
                return@setOnClickListener
            //generazione dell'id del messaggio
            val id = (2 * Chat.k) + 1
            Chat.k = id

            //inserimento del messaggio nella lista dei messaggi
            val newMessage = MyMessage(id, text, System.currentTimeMillis())
            Chat.addMessage(newMessage)

            //aggiornamento dell'adapter
            updateAdapter()

            inputText.setText("")

            //Risposta da parte dell'assistente: stesso meccanismo appena svolto
            Assistant.recogniseFunction(newMessage, this)
        }
    }

    /*//pulsante di back di sistema
    override fun onBackPressed() {
        finish()
    }*/

    override fun onResume()
    {
        called = true //l'activity ora è attiva
        val messagesR : MutableList<MyMessage> =  mutableListOf()
        getMessageWithoutRif(messagesR)
        //aggiornamento dell'adapter
        updateAdapter()

        //si sposta la lista in corrispondenza dell'ultimo elemento
        myAdapter?.itemCount?.let { n -> messRecyclerView?.smoothScrollToPosition(n) }
        super.onResume()
    }

    override fun onPause() {
        called = false //l'activity ora non è attiva, quindi non è necessario aggiornare l'adapter
        super.onPause()
    }

    override fun onStop() {
        called = false //l'activity ora non è attiva, quindi non è necessario aggiornare l'adapter
        super.onStop()
    }
}