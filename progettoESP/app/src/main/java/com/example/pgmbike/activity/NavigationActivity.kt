package com.example.pgmbike.activity


import android.annotation.SuppressLint
import android.content.*
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.pgmbike.Chat
import com.example.pgmbike.MyMessage
import com.example.pgmbike.PGMCostant
import com.example.pgmbike.R
import com.example.pgmbike.database.MyOpenHelperDB
import com.example.pgmbike.notifiche.MessageNotificationBuilder
import com.example.pgmbike.notifiche.PGMNotificationManager
import com.example.pgmbike.service.PGMMediaService
import com.example.pgmbike.service.TimerNotificationService
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * Activity di Navigazione, da qui è possibile avviare la navigazione, ovvero il Timer che scalerà
 * in automatico i soldi. E' inoltre possibile far partire la musica.
 */
class NavigationActivity : AppCompatActivity()
{
    companion object
    {
        val MUSIC_BUTTON="MusicButton"
    }
    /********** istanziazione degli elementi View presenti **********/
    private lateinit var timer : Chronometer
    private lateinit var act_button : Button
    private lateinit var pause_button : Button
    private lateinit var contextView : View
    private lateinit var music_button : Button
    private lateinit var back_button : Button
    private lateinit var portafoglio : TextView
    private lateinit var saldoText : TextView
    private lateinit var timeLeftTitle : TextView
    private lateinit var timeLefttext : TextView
    private lateinit var assistantButton : FloatingActionButton

    private lateinit var  timer_intent : Intent
    private lateinit var back_intent : Intent
    private lateinit var assistant_intent : Intent
    private lateinit var myDB: SQLiteDatabase
    private val myManager = PGMNotificationManager

    /********** variabili di supporto **********/
    private var running : Boolean = false
    private var paused : Boolean = false
    private var time : Long = 0
    private var saldo : Short = 0
    private val pgmPref="mypreference"
    private lateinit var sharedPreferences: SharedPreferences


    @SuppressLint("SetTextI18n") //aggiunto inntenzionalmente per evitare avvisi di concatenazione stringhe
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.navigation_activity) //istanziazione del layout
        Log.d("TAG","OnCreate${TimerNotificationService.running}")

        /********** Receiver per gli aggiornamenti del saldo corrente **********/
        class MyReceiver : BroadcastReceiver()
        {
            override fun onReceive(context: Context?, intent: Intent?)
            {
                if (intent != null)
                {
                    if(intent.action=="UPDATE") //check dell'intent
                    {
                        val saldoUI = intent.getStringExtra("saldo")
                        if (saldoUI != null)
                        {
                            //aggiornamento della view corrispondente
                            updateTimeLeftView(saldoUI.toShort())
                            saldoText.text = "$saldoUI$"
                            //se il saldo è minore della quantità da prelevare di default si torna on activity1
                            if (saldoUI.toShort() < TimerNotificationService.pull)
                            {
                                TimerNotificationService.running = false //ferma l'aggiornamento della notifica
                                goBack() //torna in Activity1
                            }
                        }
                    }
                }
            }
        }

        //regitrazionde del receiver
        val uiUpdated = MyReceiver()
        registerReceiver(uiUpdated, IntentFilter("UPDATE"))


        /********** Recupero deldle view **********/
        contextView = findViewById(R.id.navLayout)
        timer = findViewById(R.id.timer)
        act_button = findViewById(R.id.start_stop)
        pause_button = findViewById(R.id.pause_button)
        pause_button.isEnabled = false
        music_button = findViewById(R.id.music)
        portafoglio = findViewById(R.id.portafoglioText)
        saldoText = findViewById(R.id.saldo_text)
        timeLeftTitle = findViewById(R.id.timeLeftTitle)
        timeLefttext = findViewById(R.id.timeLeft)

        assistantButton = findViewById(R.id.AssistantButton)
        assistantButton.setImageResource(R.drawable.message_icon)
        assistantButton.setColorFilter(getColor(R.color.white))

        back_button = findViewById(R.id.back)

        /********** istanziazione degli intent **********/
        timer_intent = Intent(applicationContext, TimerNotificationService::class.java)
        back_intent = Intent(applicationContext, Activity1::class.java).apply { flags =  Intent.FLAG_ACTIVITY_CLEAR_TOP }
        assistant_intent = Intent(applicationContext, BubbleActivity::class.java).apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP     }

        //Inizializzazione delle sharedPreferences
        sharedPreferences = getSharedPreferences(pgmPref, Context.MODE_PRIVATE)

        /********** Ripristino del saldo corrente dal database**********/
        myDB = MyOpenHelperDB(this).writableDatabase
        if(restoreWallet().contentEquals("vuoto") )
        {
            saldo = 0
            saldoText.text = "0$"
            act_button.isEnabled = false
        }
        else
        {
            saldo = restoreWallet().toShort()
            saldoText.text = "${saldo}$"
        }

        /********** indicazione del tempo a disposizione per il viaggio **********/
        updateTimeLeftView(saldo)

        /********** Ripristino del cronometro di activity**********/
        if (TimerNotificationService.running)
        {
            Log.d("Ripristino","Running=true")
            restoreCount(TimerNotificationService.delta)
        }

        /********** Listener di act_button **********/
        act_button.setOnClickListener{
            if(!running) startCounting() else showPopUp()
        }

        /********** Listener di pause_button **********/
        pause_button.setOnClickListener{
            if(!paused) pauseCounting() else resumeCounting()
        }

        /********** Listener di music_button **********/
        music_button.setOnClickListener{
            val i = Intent(applicationContext, PGMMediaService::class.java)
            i.putExtra(MUSIC_BUTTON,true)
            startService(i)
        }

        /********** Listener di back button**********/
        /*
            Mostra il popup per la conferma di stop oppure, se l'attività
            non è ancora iniziata, torna direttamente alla Navigation
            activity
         */

        back_button.setOnClickListener{
            if(running) showPopUp()
            else goBack()
        }

        /********** Listener di per l'assistant button **********/
        /*
            Avvia l'activity della chat del bot, BubbleActivity; E' anche la
            stessa avviata e visualizzata nella bubble
         */
        assistantButton.setOnClickListener{
            startActivity(assistant_intent)
        }

    }

    override fun onResume() {
        super.onResume()
        /********** Ripristino del cronometro di activity**********/
        if (TimerNotificationService.running)
        {
            Log.d("Ripristino","Running=true")
            restoreCount(TimerNotificationService.delta)
        }

    }

    override fun onPause() {
        super.onPause()
        Log.d("NAV_ACT","onPause")
    }

    /********** metodo per gestione del back di sistema **********/
    /*
        Essendo in navigationActivity deve svolgere le stesse azioni del back button creato.
     */
    override fun onBackPressed()
    {
        if(running) showPopUp()
        else goBack()
    }

    /********** metodo per l'update della view timeLeft **********/
    /*
        Calcola il tempo a disposizione rimanente mediante il saldo e la quanità di denaro
        che viene scalata di default passato il tempo di riferimento della classe
        TimerNotificationService.
     */
    private fun updateTimeLeftView(balance : Short)
    {
        val minutesLeft = (balance/TimerNotificationService.pull) * TimerNotificationService.RIF * 1/60
        if(minutesLeft < 1) timeLefttext.text = getString(R.string.TimeLeftless1)
        else{
            val string = "circa $minutesLeft ${if (minutesLeft > 1) "minuti" else "minuto"}"
            timeLefttext.text = string
        }
    }

    /********** metodo di inizio conteggio **********/
    /*
        Azioni svolte da questo metodo :
        1- set dello zero del cronometro;
        2- start del cronometro;
        3- creazione dell'intent per l'attivazione del service di aggiornamento
           della notifica del timer e attivazione del servizio
        4- aggiornamento dei pulsanti di comando
     */
    private fun startCounting()
    {
        act_button.setText(R.string.Stop)
        time = SystemClock.elapsedRealtime()
        timer.base = time


        timer_intent.putExtra("start", time) //inserimento del tempo di start del timer negli extra dell'intent
        TimerNotificationService.enqueueWork(applicationContext, timer_intent)

        //attivazione del timer e del servizio
        timer.start()
        TimerNotificationService.running = true
        pause_button.isEnabled = true
        running = true

        //recupero referenza notifica di messaggio e lancio la nuova notifica

        var mN:MessageNotificationBuilder
        try
        {
            val mNNullable = PGMNotificationManager.getRef(PGMCostant.ID_MSG_NOT.v) as MessageNotificationBuilder?
            if(mNNullable!=null)mN=mNNullable
            else throw NullPointerException()
        }
        catch (e: NullPointerException)
        {
            //Se non è presente la notifica nel registro delle notifiche la si crea da zero
            mN=buildMsgNot()
        }

        //intent per la notifica
        val myIntent = Intent(this, BubbleActivity::class.java)
                .setAction(Intent.ACTION_VIEW)
                .apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TASK }

        //set del titolo, testo e intent della notifica
        mN.setTitleandText(getString(R.string.assistant_name),getString(R.string.travel))
        mN.setIntent(myIntent)
        myManager.setBuilder(mN)
        myManager.show(this)

        //creazione del messaggio
        val id = 2 * Chat.k
        Chat.k = id
        val message = MyMessage(id, getString(R.string.travel), System.currentTimeMillis())
        Chat.addMessage(message) //aggiungo il messaggio alla lista
        if(BubbleActivity.called) BubbleActivity.updateAdapter() //aggiorno la chat, se attiva
    }

    /********** metodo di stop del conteggio  **********/
    /* Azioni svolte da questo metodo :
        1- stop del cronometro e dell'aggiornamento della notifica
        2- aggiornamento dei pulsanti di comando
        3- apparizione di un Toast di avviso
    */
    private fun stopCounting()
    {
        act_button.setText(R.string.Start)
        //stop del timer e del servizio
        timer.stop()
        TimerNotificationService.running = false

        pause_button.setText(R.string.Pause)
        pause_button.isEnabled = false
        paused = false
        running = false
        //appare un toast quando si ferma il timer
        Toast.makeText(this, R.string.stop_text, Toast.LENGTH_LONG).show()
        time = 0
    }

    /********** metodo di pausa del conteggio  **********/
    /* Azioni svolte da questo metodo :
        1- stop del cronometro e dell'aggiornamento della notifica
        2- salvataggio del tempo contato finora dal timer
        3- aggiornamento dei pulsanti di comando
    */
    private fun pauseCounting()
    {
        timer.stop()
        TimerNotificationService.running = false
        time = SystemClock.elapsedRealtime() - time //tempo contato dal timer fino alla pausa in millisecondi
        pause_button.setText(R.string.Resume)
        paused = true
    }


    /********** metodo per riprendere il conteggio  **********/
    /* Azioni svolte da questo metodo :
        1- start del cronometro e dell'aggiornamento della notifica dal punto in cui si era interrotto
        2- salvataggio del nuovo tempo d'inizio
        3- aggiornamento dei pulsanti di comando
    */
    private fun resumeCounting()
    {
        //start del timer e del service
        timer.base = SystemClock.elapsedRealtime() - time
        timer_intent.putExtra("start", timer.base)
        timer.start()
        //avvio del service
        TimerNotificationService.running = true
        TimerNotificationService.enqueueWork(applicationContext, timer_intent)

        time = SystemClock.elapsedRealtime() - time
        pause_button.setText(R.string.Pause)
        paused = false
    }



    /********** metodo per il ripristino del saldo **********/
    private fun restoreWallet() : String
    {
        //query per il recupero del saldo
        val sqlQuery = "SELECT * FROM " + MyOpenHelperDB.TABLE_USER
        val res = myDB.rawQuery(sqlQuery, null)
        res.moveToFirst()
        val saldoDB = res.getString(res.getColumnIndex(MyOpenHelperDB.SALDO))
        res.close()
        return saldoDB
    }

    /********** metodo per il ripristino del cronometro **********/
    private fun restoreCount(elapsed_time: Long)
    {
        //ripristino del tempo sul cronometro e variabili d'appoggio
        time = SystemClock.elapsedRealtime() - elapsed_time
        timer.base = time
        timer.start()
        running = true
        paused = false

        //Ripristino della view
        pause_button.setText(R.string.Pause)
        pause_button.isEnabled = true
        act_button.setText(R.string.Stop)
    }

    /********** metodo per mostrare il popup per conferma stop del viaggio **********/
    private fun showPopUp()
    {
        pauseCounting() //metto  in pausa il conteggio, altrimenti continua a scalare i soldi
        disableEnableViews()
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.popup_layout, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val popupWindow = PopupWindow(popupView, width, height, false)
        val confirmButton = popupView.findViewById<Button>((R.id.ConfirmStop))
        val continueButton = popupView.findViewById<Button>((R.id.Continue))
        popupWindow.animationStyle = R.style.animation
        popupWindow.showAtLocation(contextView, Gravity.CENTER, 0, 0)

        //listener per il pulsante di rifiuto -> se l'utente lo preme, si continua a viaggiare
        continueButton.setOnClickListener{
            disableEnableViews()
            popupWindow.dismiss()
            resumeCounting()
        }
        //listener per il pulsante di conferma -> se l'utente lo preme, si ferma il contatore
        confirmButton.setOnClickListener{
            stopCounting()
            popupWindow.dismiss()

            //recupero referenza notifica di messaggio e lancio la nuova notifica

            var mN:MessageNotificationBuilder
            try
            {
                val mNNullable = PGMNotificationManager.getRef(PGMCostant.ID_MSG_NOT.v) as MessageNotificationBuilder?
                if(mNNullable!=null) mN = mNNullable
                else throw NullPointerException()
            }
            catch (e: NullPointerException)
            {
                //Se non è presente la notifica nel registro delle notifiche la si crea da zero
                mN = buildMsgNot()
            }

            //set del titolo, testo e intent della notifica
            mN.setTitleandText(getString(R.string.assistant_name),getString(R.string.endTravel))
            myManager.setBuilder(mN)
            myManager.show(this)

            //creazione del messaggio
            val id = 2 * Chat.k
            Chat.k = id
            val message = MyMessage(id, getString(R.string.endTravel), System.currentTimeMillis())
            Chat.addMessage(message) //aggiungo il messaggio alla lista
            if(BubbleActivity.called) BubbleActivity.updateAdapter() //aggiorno la chat, se attiva

            goBack() //ritorno all'activity1
        }
    }


    /********** metodo per ritornare all'activity 1 **********/
    private fun goBack()
    {
        //Memorizzo che NavigationActivity è terminata
        val edit=sharedPreferences.edit()
        edit.putBoolean(PGMCostant.KEY_NAVIGATION_LAUNCHED.v.toString(),false)
        edit.apply()
        startActivity(back_intent)
        finish()
    }

    /********** abilitazione/disabilitazione dei pulsanti dell'interfaccia **********/
    private fun disableEnableViews()
    {
        pause_button.isEnabled = !pause_button.isEnabled
        act_button.isEnabled = !act_button.isEnabled
        back_button.isEnabled = !back_button.isEnabled
        assistantButton.isEnabled = !assistantButton.isEnabled
        music_button.isEnabled = !music_button.isEnabled
    }

    /********** costruzione notifica messaggistica (se riferimento non presente)  **********/
    private fun buildMsgNot():MessageNotificationBuilder
    {
        //NOTIFICA MESSAGGISTICA
        val mN= MessageNotificationBuilder(
                this,
                PGMCostant.MESG_CHANNEL.v,
                getString(R.string.MSG_CH_NAME),
                getString(R.string.MSG_CH_DESC)
        )
        val myIntent = Intent(this, BubbleActivity::class.java)
                .setAction(Intent.ACTION_VIEW)
                .apply {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
        mN.setIntent(myIntent)
        PGMNotificationManager.register(PGMCostant.ID_MSG_NOT.v,mN) //Registro la referenza di quel costruttore

        return mN
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle)
    {
        savedInstanceState.putString("Start&Stop", act_button.text.toString())
        super.onSaveInstanceState(savedInstanceState)
    }
}