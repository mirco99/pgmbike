package com.example.pgmbike.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

/**
 * Database che contiene i dati dell'utente e diverisi codici bici validi
 */
class MyOpenHelperDB(c: Context): SQLiteOpenHelper(c, DB_NAME,null, DB_VERSION)
{
    /**
     * Metodo eseguito solo se il database deve essere creato
     */
    override fun onCreate(db: SQLiteDatabase)
    {
        /**
         * Statement che costruisce la tabella che contiene
         * i codici di tutte le bici disponibili
         */
        /**
         * Statement sql
         */
         var sql = "create table " + TABLE + "( " + BaseColumns._ID +
                " integer primary key autoincrement, " + COD + " char );"
        db.execSQL(sql)

        //Contenitore dei dati
        val codBike= ContentValues()
        //popolamento di Bikedatabase
        for(i in 1..100)
        {
            codBike.put(COD,(i*5).toString())               //Codice bici i
            db.insert(TABLE,null, codBike)    //Inserimento del database
        }

        /**
         * Costruzione della tabella per l'utente
         */
        sql="create table "+
                TABLE_USER +" ( _id integer primary key autoincrement, "+
                NOME +" char, "+
                COGNOME +" char, "+
                BIRTH +" char, "+
                EMAIL +" char, "+
                PSW +" char, "+
                SALDO +" char );"

        db.execSQL(sql)

        val tmp=ContentValues()
        tmp.put(NOME,"noNome")  //Inserimento con dati non validi di default
        tmp.put(COGNOME,"noCognome")
        tmp.put(SALDO,"vuoto")
        db.insert(TABLE_USER,null, tmp)
    }

    /**
     * Non è previsto un cambio di versione
     */
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int)
    {
        return
    }

    companion object
    {
        //DATABASE BICI
        private const val DB_NAME="Bikedatabase.db"
        private const val DB_VERSION=2
        const val TABLE="BikeCode"
        const val COD="codice"

        //DATABASE UTENTE
        const val TABLE_USER="Utente"
        const val NOME="Nome"
        const val COGNOME="Cognome"
        const val EMAIL="email"
        const val PSW="password"
        const val SALDO="Saldo"
        const val BIRTH="DataNascita"

    }
}