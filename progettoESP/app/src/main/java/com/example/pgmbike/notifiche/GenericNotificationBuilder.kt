package com.example.pgmbike.notifiche

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.example.pgmbike.R

/**
 * Builder utilizzato per costruire una notifica generica, ovvero che non è associata a nessun stile
 */

class GenericNotificationBuilder (c: Context, val chID: Int = 0, chName: String = "name", desc: String = "desc")
    : NotificationBuilder(context = c, channelID = chID, channelName = chName, channelDescription = desc)
{
    private var timer_style : Boolean = false       //Se vero la notifica usa delle impostazioni diverse

    /*INTENT per il tocco*/
    private val TAP_INTENT = 0
    private var pendingIntent : PendingIntent? = null

    //Testo che compare quando si espande la notifica
    private var notBigText:String?=null

    /**
     * Costuzione della notifica generica
     */
    override fun buildNotification(): Notification {
        val builder=NotificationCompat.Builder(context, chID.toString())
            .setSmallIcon(R.mipmap.ic_pgmbike_foreground)
            .setAutoCancel(true)
            .setContentTitle(title)
            .setContentText(text)
            .setStyle(NotificationCompat.BigTextStyle().bigText(notBigText))
                .apply {
                    if (timer_style) //codice relativo alle notifiche del timer
                    {
                        setOnlyAlertOnce(true)
                        setShowWhen(false)
                        setSmallIcon(R.drawable.timer)
                        setOngoing(true)
                    }
                }
        //Se un utente ha assegnato l'intet lo si assegna alla notifica altrimenti la notifica verrà creata ugualmente
        //ma senza rispondere al tocco
        if(pendingIntent!=null)builder.setContentIntent(pendingIntent)
        return builder.build()
    }


    /**
     * Set del testo che compare quando si espande la notifica
     */
    fun setBigText(BigText: String)
    {
        notBigText = BigText
    }

    /**
     * Titolo e testo notifica
     */
    override fun setTitleandText(title: String, text:String)
    {
        this.title = title
        this.text = text
    }

    /**
     * Intent per la notifica
     */
    override fun setIntent(myIntent: Intent)
    {
        pendingIntent = PendingIntent.getActivity(context, TAP_INTENT, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    //ritorna una descrizione della notifica
     override fun toString():String
    {
        return title
    }

    /**
     * Se vero la notifica usa delle impostazioni diverse quando costruita
     */
    fun setTimerStyle(flag : Boolean)
    {
        timer_style = flag
    }
}