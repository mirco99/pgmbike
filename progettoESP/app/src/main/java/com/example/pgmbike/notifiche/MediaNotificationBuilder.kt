package com.example.pgmbike.notifiche

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.core.app.NotificationCompat
import androidx.media.app.NotificationCompat.MediaStyle
import androidx.media.session.MediaButtonReceiver
import com.example.pgmbike.R

/**
 * Builder il cui scopo è di creare una notifica che permette di visualizzare e interagire con un media musicale
 */

//Builder per notifiche di media
class MediaNotificationBuilder (private val c: Context, val chID: Int=0, private val chName: String="name", desc: String="desc")
    : NotificationBuilder(context = c, channelID = chID, channelName = chName, channelDescription = desc)
{

    companion object
    {
        private const val TAP_MEDIA = 1
        private const val PLAY_BUTTON = 0
        private const val PAUSE_BUTTON = 1
        private const val STOP_BUTTON = 2
    }

    /**
     * Azioni legate ai pulsanti sulla notifica
     */
    //Azione per far partire il brano
    private val actionPlay=NotificationCompat.Action(R.mipmap.ic_play_media_service_foreground,"Play",
            MediaButtonReceiver.buildMediaButtonPendingIntent(c,PlaybackStateCompat.ACTION_PLAY))

    //Azione per mettere in pausa il brano
    private val actionPause=NotificationCompat.Action(R.mipmap.ic_pause_media_service_foreground,"Pausa",
            MediaButtonReceiver.buildMediaButtonPendingIntent(c,PlaybackStateCompat.ACTION_PAUSE))

    //Azione per fermare e chiudere la riproduzione
    private val actionStop=NotificationCompat.Action(R.mipmap.ic_stop_media_service_foreground,"Stop",
            MediaButtonReceiver.buildMediaButtonPendingIntent(c,PlaybackStateCompat.ACTION_STOP))

    /**
     *  Media session permette di interegire con i media
     *  In questo caso è una mediaSession vuota finchè l'utente non definisce la sua
     */
    private var myMediaSession=MediaSessionCompat(c,MediaSessionCompat.ARGUMENT_MEDIA_ATTRIBUTE)
    //Intenti di tocco
    private var tapPendingIntent:PendingIntent?=null

    //Assegnazione della mediaSession
    fun setMediaSession(mediaSession:MediaSessionCompat)
    {
        myMediaSession=mediaSession
    }

    //Intent per il tocco della notifica
    override fun setIntent(myIntent: Intent)
    {
        tapPendingIntent=PendingIntent.getActivity(
                c,
                TAP_MEDIA,
                myIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)
    }

    /**
     * Costruzione della notifica
     */
    override fun buildNotification(): Notification
    {
        //Ripresa del mediaController assocciato alla mediaSession
        val mediaController=MediaControllerCompat(c, myMediaSession)
        //Recupero il playBackState
        val playBackState=mediaController.playbackState

        //Costruizione della notifica
        val builder= NotificationCompat.Builder(c, chID.toString())
                //Azioni
                .addAction(actionPlay)  //indice PLAY_BUTTON
                .addAction(actionPause) //indice PAUSE_BUTTON
                .addAction(actionStop)  //indice STOP_BUTTON
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setSmallIcon(R.mipmap.ic_play_media_service_foreground)
                .setStyle(
                        MediaStyle()
                        //Visualizzazione in forma compatta dei pulsanti indicizzati
                        .setShowActionsInCompactView(
                                //A seconda dello stato del playBackState si imposta un solo pulsante da
                                //visualizzare in forma compatta
                                when(playBackState.state)
                                {
                                    PlaybackStateCompat.STATE_PLAYING -> PAUSE_BUTTON
                                    PlaybackStateCompat.STATE_PAUSED -> PLAY_BUTTON
                                    else -> STOP_BUTTON
                                }
                        )
                        //Assegnazione della mediaSession
                        .setMediaSession(myMediaSession.sessionToken)
                )
                //Si rende la notifica visibile anche nella schermata di blocco
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

        //L'intent di tocco viene impostato solo se è stato inizializzato da qualcuno
        if(tapPendingIntent!=null) builder.setContentIntent(tapPendingIntent)
        return builder.build()
    }
}


