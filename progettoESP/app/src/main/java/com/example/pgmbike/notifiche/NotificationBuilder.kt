package com.example.pgmbike.notifiche

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import androidx.core.app.NotificationCompat
import android.content.Context
import android.content.Intent

/**
 * Classe astratta di base per la costruzione di una notifica, chi eredita questa classe ottiene la costruzione automatica
 * del canale.
 * Richiede come parametri ID, nome e descrizione del canale.
 * Un ID pari -> la notifica sarà ad alta priorità.
 * Un ID dispari -> la notifica sarà a bassa priorità.
 * Il metodo buildNotification verrà invocato dal PGMNotificationManager che costruirà la notifica e la mostrerà,
 * va implementato in modo diverso per ogni notifica che si intende creare
 *
 * DESIGN PATTERN BUILDER
 */

abstract class NotificationBuilder(val context:Context,                     /*Contesto passato dalla classe chiamante*/
                                   val channelID:Int=0,                     /*ID canale*/
                                   val channelName: String="name",          /*nome canale*/
                                   val channelDescription: String="Desc"   /*Descrizione del canale*/
                                  )
{
    init
    {
        /*Costruizione del canale*/
        mycreateNotificationChannel()
    }

    open var title:String="titolo"   /*Titolo per la notifica*/
    open var text:String="testo"     /*Testo per la notifica*/

    /*METODI ASTRATTI*/
    /**
     * Metodo per la costruzione della notifica
     */
    open fun buildNotification(): Notification
    {
        //Bisonga inserire tutti i campi
        return NotificationCompat.Builder(context,channelID.toString())
                .setContentTitle(title)
                .build()

    }
    /*Titolo e testo notifica*/
    open fun setTitleandText(title: String, text:String){}

    /*Intent per la notifica*/
    open fun setIntent(myIntent: Intent){}

    //ritorna una descrizione della notifica
    override fun toString():String{
        return "none"
    }

    /**
     * Costruzione del canale
     */
    private fun mycreateNotificationChannel()
    {
        //La priorità del canale viene cambiata in base al numero del canale
        //Canale pari -> priorità alta
        //Canale dispari -> priorità bassa
        var importance = NotificationManager.IMPORTANCE_HIGH   /*Con HIGH la notifica appare a schermo*/
        if(channelID%2 != 0) importance = NotificationManager.IMPORTANCE_LOW
        val channel = NotificationChannel(
                channelID.toString(),
                channelName,
                importance).apply {
            description = channelDescription
        }
        PGMNotificationManager.getNotificationManager(context).createNotificationChannel(channel)
    }
}