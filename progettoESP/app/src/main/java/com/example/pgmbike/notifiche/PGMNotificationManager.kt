package com.example.pgmbike.notifiche

import android.app.Notification
import android.content.Context
import androidx.core.app.NotificationManagerCompat

/**
 * Manager per le notifiche, in questa classe ci sono tutta una serie di metodi che aiutano nella gestione delle notifiche
 * E' il DIRECTOR che gestisce tutti i builder che vengono creati ereditando NotificationBuilder
 */

//Con object si implementa un DP Singleton
object PGMNotificationManager{
    //Mappa che contiene le referenze agli oggetti notiche creati
    var registeredNotification= mutableMapOf<Int, NotificationBuilder>()

    private lateinit var builder: NotificationBuilder                   //Referenza alla notifica da costruire
    private var myNotificationManager:NotificationManagerCompat?=null   //Referenza al NotificationManager

    //Definisce il tipo di notifica da costruire
    fun setBuilder(b: NotificationBuilder)
    {
        builder =b
    }

    //Costruisce la notifica
    fun build():Notification
    {
        return builder.buildNotification()
    }

    /**
     * Si registra il tipo di notifca dentro la mappa
     * Ritorna falso se l'ID è già presente oppure quell'ID è già stato inserito.
     * Non è l'ID del canale, ma l'ID della notifica, cioè identifica il tipo di notifica all'interno dell'app
     */
    fun register(ID: Int, n: NotificationBuilder): Boolean
    {
        //La notifica viene registrata solo se quell'ID non corrispone ad un'altra notifica già presente
        if(registeredNotification[ID] == null)
        {
            registeredNotification[ID] = n
            return true
        }
        return false
    }

    /**
     * Ritorna la referenza alla notifica associata con l'ID, null se non è presente
     */
    fun getRef(ID:Int): NotificationBuilder?
    {
        return registeredNotification[ID]
    }

    /**
     * Rimuove la referenza alla notifica associata con l'ID
     */
    fun removeRef(ID: Int): NotificationBuilder?
    {
        return  registeredNotification.remove(ID)
    }

    /**
     * Ritorna una referenza al NotificationManagerCompat
     */
    fun getNotificationManager(c: Context): NotificationManagerCompat
    {
        if(myNotificationManager ==null){
           myNotificationManager = NotificationManagerCompat.from(c) //Ritorna l'oggetto NotificationManagerCompat
        }
        return myNotificationManager as NotificationManagerCompat
    }

    /**
     * Visualizza la notifica
     */
    fun show(c: Context)
    {
        //Si ottiene una referenza valida al NotificationManager se nessuno prima lo ha fatto
        if(myNotificationManager ==null) getNotificationManager(c)
        myNotificationManager?.notify(builder.channelID, build())
    }

}