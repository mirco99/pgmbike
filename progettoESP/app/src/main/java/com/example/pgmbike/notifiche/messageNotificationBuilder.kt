package com.example.pgmbike.notifiche

import android.app.Notification
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.Person
import androidx.core.app.RemoteInput
import androidx.core.content.LocusIdCompat
import androidx.core.content.pm.ShortcutInfoCompat
import androidx.core.content.pm.ShortcutManagerCompat
import androidx.core.graphics.drawable.IconCompat
import com.example.pgmbike.PGMCostant
import com.example.pgmbike.R
import com.example.pgmbike.ReplyReceiver
import com.example.pgmbike.activity.Activity1


class MessageNotificationBuilder(   val c: Context,
                                    val chID: Int = 0,
                                    chName: String = "test",
                                    chDesc: String = "test")
    : NotificationBuilder(context=c, channelID=chID, channelName=chName, channelDescription=chDesc)
{

    override var title : String = "Bicycle assistant"
    override var text : String = "Ehi there! This is a Bubble notification!"
    private var BOT_NAME : String = "Assistant"

    /********** INTENT DI NOTIFICA **********/
    private lateinit var bubbleIntent : Intent
    private lateinit var pendingIntent : PendingIntent
    private val PEND_REQ : Int = 1

    /********** CREAZIONE PERSONA **********/
    private val icon = IconCompat.createWithResource(c, R.drawable.bot)
    private val assistant = Person.Builder().setName(BOT_NAME).setIcon(icon).build()
    private lateinit var msg : NotificationCompat.MessagingStyle.Message

    /********** SHORTCUT : necessaria per visualizzare la notifica in bubble **********/
    private val short_icon = IconCompat.createWithResource(c, R.drawable.bot)
    private val SHORTCUT_ID : String = chID.toString()
    private lateinit var shortcut : ShortcutInfoCompat

    /********** METADATA : necessario per Bubble **********/
    private val bubble_icon = IconCompat.createWithResource(c, R.drawable.bot)
    private lateinit var bubbleMeta : NotificationCompat.BubbleMetadata

    /********** PULSANTE DI RISPOSTA (REPLY) **********/
    private val replyLabel : String = context.getString(R.string.reply_button)
    private lateinit var replIntent : Intent
    private lateinit var remoteInput : RemoteInput
    private lateinit var replyPendingIntent : PendingIntent
    private lateinit var action : NotificationCompat.Action

    /********** NOTIFICA **********/
    private lateinit var notifica : Notification


    /********** COSTRUZIONE DELLA NOTIFICA DELL'ASSISTENTE **********/
    override fun buildNotification(): Notification
    {
        setMessage() //imposta il messaggio da visualizzare
        buildReply() //costruisce il pulsante di reply

        //Costruzione della notifica
        notifica = NotificationCompat.Builder(c,chID.toString())
                .setSmallIcon(R.drawable.message_icon) // //icona piccola a fianco di quella più grande
                .setContentIntent(pendingIntent)  //al click della notifica veniamo mandati in BubbleActivity
                .setStyle(NotificationCompat.MessagingStyle(assistant)
                        .setConversationTitle(title)
                        .addMessage(msg))
                .setAutoCancel(true)
                .addAction(action)  //aggiunta del pulsante di reply
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .apply {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R)  //codice condizionale per versioni >= Android 11 (API LEVEL 30)
                    {
                        buildShortcut() //costruzione shortcut
                        buildMetaData() //costruzione metdata
                        setShortcutId(SHORTCUT_ID)
                        bubbleMetadata = bubbleMeta  //fa sì che l'activity venga mostrata in una finestra ridimensionata e flottante
                    }
                }
                .build()
        return notifica
    }

    /********** SET DEL TITOLO E DEL TESTO DELLA NOTIFICA **********/
    override fun setTitleandText(title: String, text: String)
    {
        this.title = title
        this.text = text
        super.setTitleandText(title, text)
    }

    /********** SET DELL'INTENT AL TOCCO DELLA NOTIFICA **********/
    override fun setIntent(myIntent: Intent)
    {
        bubbleIntent = myIntent
        pendingIntent = PendingIntent.getActivity(c, PEND_REQ, bubbleIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        super.setIntent(myIntent)
    }

    /********** METODO toString **********/
    override fun toString(): String
    {
        return "$title : $text"
    }

    /********** COSTRUZIONE PULSANTE REPLY **********/
    private fun buildReply()
    {
        remoteInput  = RemoteInput.Builder(ReplyReceiver.KEY_TEXT_REPLY).setLabel("Reply").build()
        replIntent = Intent(c, ReplyReceiver::class.java)
        replyPendingIntent= PendingIntent.getBroadcast(c, PGMCostant.REPL_REQ.v, replIntent ,PendingIntent.FLAG_UPDATE_CURRENT)
        action = NotificationCompat.Action.Builder(R.drawable.send_icon, replyLabel, replyPendingIntent)
                .addRemoteInput(remoteInput)
                .setAllowGeneratedReplies(true)
                .build()
    }

    /********** COSTRUZIONE DELLA SHORTCUT **********/
    private fun buildShortcut()
    {
        shortcut = ShortcutInfoCompat.Builder(c, SHORTCUT_ID)
                .setLongLived(true) //necessaria per essere conversation notification
                .setShortLabel(title)
                .setActivity(ComponentName(c, Activity1::class.java)) //lancio della bubbleActivity da shortcut
                .setIntent(bubbleIntent) //quando clicco la notifica esegue bubbleIntent (bubbleIntent apre la chat dell'assistente)
                .setIcon(short_icon)     //icona dello shortcut (quando la bubble è "contratta")
                .setPerson(assistant)
                .setLocusId(LocusIdCompat(SHORTCUT_ID)) //permette di tenere traccia delle conversazioni frequenti
                .build()

        ShortcutManagerCompat.pushDynamicShortcut(c, shortcut) //"pubblicazione" della shortcut per il sistema

        //Altro modo per pubblicare la shortcut (non necessita dependencies):

        /*val shortcutList = mutableListOf(shortcut)
        getSystemService(c, ShortcutManagerCompat::class.java)
        ShortcutManagerCompat.addDynamicShortcuts(c, shortcutList)*/
    }

    /********** COSTRUZIONE DELLA METADATA **********/
    private fun buildMetaData()
    {
        bubbleMeta = NotificationCompat.BubbleMetadata.Builder(pendingIntent, bubble_icon)
                .setDesiredHeight(800)     //altezza dell'activity nella bubble : ATTENZIONE : AssistantChatActivity dev'essere resizable  --> settare questa cosa nel manifest
                .build()
    }

    /********** SET DEL MESSAGGIO **********/
    private fun setMessage()
    {
        /*
        Messaggio di testo con :
            - testo del messaggio;
            - ora di ricezione del messaggio.
            - nome del contatto : assistant;
        */
        msg = NotificationCompat.MessagingStyle.Message(text, System.currentTimeMillis(), assistant)
    }
}











