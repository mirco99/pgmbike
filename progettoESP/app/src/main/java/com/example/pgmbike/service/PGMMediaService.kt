package com.example.pgmbike.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.media.AudioManager
import android.os.IBinder
import android.os.PowerManager
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.media.session.MediaButtonReceiver
import androidx.media.AudioFocusRequestCompat
import androidx.media.AudioManagerCompat
import com.example.pgmbike.PGMCostant
import com.example.pgmbike.R
import com.example.pgmbike.activity.Activity1
import com.example.pgmbike.activity.NavigationActivity
import com.example.pgmbike.notifiche.MediaNotificationBuilder
import com.example.pgmbike.notifiche.PGMNotificationManager

class PGMMediaService: Service() {

    //Callback associate alla media session
    inner class MediaCallback: MediaSessionCompat.Callback() {
        override fun onPause()
        {
            pause()
            super.onPause()
        }

        override fun onStop()
        {
            AudioManagerCompat.abandonAudioFocusRequest(myAudioManager,audioRequest)
            stop()
            super.onStop()
        }

        override fun onPlay()
        {
            //Richiesta dell'AudioFocus
            val res=AudioManagerCompat.requestAudioFocus(myAudioManager,audioRequest)
            //Se viene negata non si avvia il mediaPlayer
            if(res==AudioManager.AUDIOFOCUS_REQUEST_FAILED ||
                res==AudioManager.AUDIOFOCUS_REQUEST_DELAYED) return

            //Avvio del MediaPlayer solo in caso di permesso concesso
            play()

            super.onPlay()
        }

        //pos -> posizione a cui portare il mediaPLayer in ms
        override fun onSeekTo(pos: Long)
        {
            super.onSeekTo(pos)
            seekTo(pos.toInt())

            //Reimpostazione dello stato così da aggiornare la barra di progresso della notifica
            buildPlayBackState(PlaybackStateCompat.STATE_PLAYING)
            //Aggiorno la notifica
            PGMNotificationManager.setBuilder(mediaNot)
            PGMNotificationManager.show(applicationContext)
        }
    }
    /**Media Session Callback*/
    private var myCallback=MediaCallback()

    ///Variabili per la mediaSession
    private lateinit var mediaSession: MediaSessionCompat
    private var stateBuilder: PlaybackStateCompat.Builder= PlaybackStateCompat.Builder()

    //Notifica
    private lateinit var mediaNot: MediaNotificationBuilder
    private var channelId=0 //Id del canale

    //MediaPlayer
    private lateinit var myMediaPlayer:MediaPlayer
    private var isPlaying:Boolean=false
    private var isPause:Boolean=false
    private var durata:Long=0

    //Listener invocato al termine di un brano
    inner class ConcreteOnCompleteListener:MediaPlayer.OnCompletionListener
    {
        override fun onCompletion(mp: MediaPlayer?) {
            mp?.seekTo(0)
            //Reimposto la barra della notifica
            buildPlayBackState(PlaybackStateCompat.STATE_PLAYING)

            //Aggiorno la notifica
            PGMNotificationManager.setBuilder(mediaNot)
            PGMNotificationManager.show(applicationContext)

            //Riavvio del mediaPlayer
            mp?.start()
        }
    }
    /**OnComplete Listener*/
    private val compListener=ConcreteOnCompleteListener()

    //AudioFocus
    private lateinit var audioRequest:AudioFocusRequestCompat
    private lateinit var myAudioManager:AudioManager
    //Callback invocata nel momento in cui cambia l'AudioFocus
    inner class ConcreteAfListener:AudioManager.OnAudioFocusChangeListener
    {
        override fun onAudioFocusChange(focusChange: Int) {
            when(focusChange)
            {
                //AudioFocus concesso
                AudioManager.AUDIOFOCUS_GAIN-> play()

                //Temporanea perdita dell'audio focus
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT->pause()

                //Perdita di durata indeterminata dell'audio focus
                AudioManager.AUDIOFOCUS_LOSS-> stop()
                else -> stop()
            }
        }
    }
    /**Audio Focus Listener*/
    private val afListener=ConcreteAfListener()

    //Non è previsto l'avvio del Service in modalità BOUND
    override fun onBind(intent: Intent?): IBinder?
    {
        return null
    }

    //Creazione del Service
    override fun onCreate()
    {
        super.onCreate()
        /**
         * Costruzione della notifica
         * Costruizione della Media Session
         */

        /**
         * Si tenta di recuperare la referenza dal PGMNotificationManager,
         * se non è possibile si crea la notifica da zero
         */
        mediaNot = try{
            //Ripresa della referenza al MediaNotificationBuilder la PGMNotificationManagaer
            val tryMediaNot= PGMNotificationManager.getRef(PGMCostant.ID_MEDIA_NOT.v) as MediaNotificationBuilder?
            if (tryMediaNot != null) {
                tryMediaNot
            }
            //Se il NotificationManager restituisce null si lancia un eccezione
            else throw NullPointerException()
        }
        catch (e: NullPointerException){
            //Se viene lanciata un eccezzione si costruisce la notifica
            buildMediaNot()
        }
        //Intent per il tocco della notifica
        val tapIntent = Intent(this, Activity1::class.java)
        mediaNot.setIntent(tapIntent)

        channelId=mediaNot.chID //Ripresa dell'ID canale notifica

        //Costruzione della MediaSession
        mediaSession = MediaSessionCompat(applicationContext, /*TAG*/MediaSessionCompat.ARGUMENT_MEDIA_ATTRIBUTE)

        //Richiesta dell'audio manager
        myAudioManager=getSystemService(Context.AUDIO_SERVICE) as AudioManager

        //AudioFocusRequestBuilder
        //Costruzione di una richiesta di AudioFocus di durata indefinita
        audioRequest=AudioFocusRequestCompat.Builder(AudioManagerCompat.AUDIOFOCUS_GAIN)
                .setOnAudioFocusChangeListener(afListener)
                .build()

    }

    //Avvio del Serivice
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int
    {
        /**
         *  AVVIO DEL MEDIAPLAYER DA PARTE DI STARTSERVICE()
         */
        if(intent!=null)
        {
            if(intent.getBooleanExtra(NavigationActivity.MUSIC_BUTTON,false))play()
        }

        /**
         * INTERAZIONE CON IL MEDIAPLAYER DA PARTE DEL MEDIA BUTTON
         *  handleInten invoca il metodo MediaController.dispatchMediaButtonEvent(keyEvent) per
         *  triggerare la mediaSessionCallback associata con il KeyEvent
         */
        MediaButtonReceiver.handleIntent(mediaSession,intent)
        return START_STICKY
    }

    //Avvio o riavvio del MediaPlayer
    private fun play()
    {
        if (isPlaying)
        {
            /**
             * Si riavvia la macchina a stati nel caso in cui il mediaPlayer sia in pausa
             */
            if(isPause)
            {
                myMediaPlayer.start()
                buildPlayBackState(PlaybackStateCompat.STATE_PLAYING)
                isPause=false

                //Aggiorno la notifica
                PGMNotificationManager.setBuilder(mediaNot)
                PGMNotificationManager.show(applicationContext)
            }
            return
        }
        /**
         * Avvio da zero del mediaPlayer e impostazione della notifica
         */
        isPlaying = true
        //Con il metodo create si porta la macchina a stati di mediaPlayer direttamente
        //alla fine dell'inizializzazione
        myMediaPlayer = MediaPlayer.create(this, R.raw.thebeginning)
        myMediaPlayer.setOnCompletionListener(compListener)

        //Durata del brano
        durata= myMediaPlayer.duration.toLong()
        myMediaPlayer.isLooping = false //In questo modo viene invocato onCOmpleteListener

        //In questo modo la CPU non va in uno stato di riposo
        myMediaPlayer.setWakeMode(applicationContext, PowerManager.PARTIAL_WAKE_LOCK)

        myMediaPlayer.start()

        //Inizializzazione della MediaSession
        initMediaSession()
        mediaSession.isActive=true

        //Mostro la notifica
        PGMNotificationManager.setBuilder(mediaNot)
        //Dichiaro il service importante per il forground
        startForeground(PGMCostant.MEDIA_CHANNEL.v, PGMNotificationManager.build())
    }

    //Pausa del MediaPlayer
    private fun pause()
    {
        //Se non è in play ritorno
        if (!isPlaying) return
        isPause=true
        //Macchina a stati di media player in PAUSED
        myMediaPlayer.pause()
        //Imposto la mediaSession in Pausa
        buildPlayBackState(PlaybackStateCompat.STATE_PAUSED)

        //Aggiorno la notifica
        PGMNotificationManager.setBuilder(mediaNot)
        PGMNotificationManager.show(applicationContext)
    }

    //Stop del MediaPlayer
    private fun stop()
    {
        if (isPlaying) {
            isPlaying = false
            //Macchina a stati di media player in STOPPED

            buildPlayBackState(PlaybackStateCompat.STATE_PAUSED)
            mediaSession.isActive=false
            myMediaPlayer.stop()
            myMediaPlayer.release()
            //Aggiorno la notifica
            PGMNotificationManager.setBuilder(mediaNot)
            PGMNotificationManager.show(applicationContext)

            stopForeground(false)
        }
    }

    //Cambio della posizione di riproduzione
    private fun seekTo(pos: Int){
        if(isPlaying)myMediaPlayer.seekTo(pos)
    }

    //Distruzione del Service
    override fun onDestroy()
    {
        stop()
        super.onDestroy()
    }

    //Inizializzazione della mediaSession
    private fun initMediaSession()
    {
        /**
         * COSTRUZIONE DELLA MEDIA SESSION
         * Costruzione del PlayBackState
         * Costruzione dei dati associati alla mediaSession
         * Associazione della media Session alla notifica
         */

        //Registro della Callback ovvero di quei metodi che possono essere chiamati da componenti esterni
        //per interagire con la mediaSession
        mediaSession.setCallback(myCallback)

        //Imposto il PlayBack come in play
        buildPlayBackState(PlaybackStateCompat.STATE_PLAYING)

        //Contenitore di dati
        val metadataBuilder = MediaMetadataCompat.Builder()
        //Imposta i dati che descrivono la mediaSession
        mediaSession.setMetadata(metadataBuilder.apply {
            //TITOLO DEL BRANO
            putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, "The Begginning")
            //ARTISTA
            putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "Lost European")
            //Durata in ms
            putLong(MediaMetadataCompat.METADATA_KEY_DURATION,durata)
            //Icona del brano
            putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART,BitmapFactory.decodeResource(resources,R.mipmap.ic_pgmbike_foreground))
        }.build())
        //Assegno la media Session alla notifica
        mediaNot.setMediaSession(mediaSession)
    }

    //Costruzione del PlayBackState
    //Il parametro specifica lo stato della riproduzione ( in pausa, play, stop..)
    //Lo stato deve essere una delle costanti definite nella classe PlaybackStateCompat
    private fun buildPlayBackState(state: Int)
    {
        //Costruzione del PlayBackState
        stateBuilder.setActions(
                    PlaybackStateCompat.ACTION_PLAY
                    or PlaybackStateCompat.ACTION_STOP
                    or PlaybackStateCompat.ACTION_PAUSE
                    or PlaybackStateCompat.ACTION_SEEK_TO
        )

        /**
         * Lo stato determina se la barra di scorrimento prosegue oppure no
         */
        when(state){
            PlaybackStateCompat.STATE_PLAYING ->
                stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING,myMediaPlayer.currentPosition.toLong(),1.0f)
            PlaybackStateCompat.STATE_PAUSED ->
                stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,myMediaPlayer.currentPosition.toLong(),1.0f)
            PlaybackStateCompat.STATE_STOPPED ->
                stateBuilder.setState(PlaybackStateCompat.STATE_STOPPED,myMediaPlayer.currentPosition.toLong(),1.0f)
        }
        //Costruizione del PlayBackState e associazione alla mediaSession
        mediaSession.setPlaybackState(stateBuilder.build())
    }

    /**
     * Costruzione della notifica per i Media se non viene recuperata con successo
     */
    private fun buildMediaNot():MediaNotificationBuilder
    {
        //NOTIFICA PER I MEDIA
        val mediaNot = MediaNotificationBuilder(
                this,
                PGMCostant.MEDIA_CHANNEL.v,
                getString(R.string.MEDIA_CH_NAME),
                getString(R.string.MEDIA_CH_DESC))
        //Registro la referenza di quel costruttore
        PGMNotificationManager.register(PGMCostant.ID_MEDIA_NOT.v,mediaNot)
        return mediaNot
    }
}