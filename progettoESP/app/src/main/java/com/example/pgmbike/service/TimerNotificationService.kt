package com.example.pgmbike.service


import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.SystemClock
import androidx.core.app.JobIntentService
import com.example.pgmbike.Chat
import com.example.pgmbike.MyMessage
import com.example.pgmbike.PGMCostant
import com.example.pgmbike.R
import com.example.pgmbike.activity.Activity1
import com.example.pgmbike.activity.BubbleActivity
import com.example.pgmbike.database.MyOpenHelperDB
import com.example.pgmbike.notifiche.GenericNotificationBuilder
import com.example.pgmbike.notifiche.MessageNotificationBuilder
import com.example.pgmbike.notifiche.NotificationBuilder
import com.example.pgmbike.notifiche.PGMNotificationManager


/**
     Classe per il service per l'aggiornamento di notifica.
     Per fare l'update della notifica ogni secondo è necessario fermare il thread corrente per
     1 secondo: per fare ciò activity e serivice devono essere eseguiti in thread separati,
     altrimenti l'applicazione si blocca.
     Per fare ciò, viene utilizzata la classe JobIntentService che permette l'esecuzione del servizio
     in un thread separato da quello principale.
 */

class TimerNotificationService : JobIntentService()
{
    private var saldo : Short = 0
    private lateinit var myDB: SQLiteDatabase


    companion object
    {
        var running: Boolean = false //se vero -> timer funzionante, falso -> timer fermo
        const val pull : Short = 2 //quantità da scalare dal saldo
        var RIF  : Short = 10 //quantità di tempo in s di riferimento (ogni quanto prelevare) < 60s
        var delta : Long = 0 //tempo trascorso dall'avvio del timer in millisecondi

        private val JOB_ID = PGMCostant.JOB_ID.v

        fun enqueueWork(context: Context, intent: Intent)
        {
            enqueueWork(context, TimerNotificationService::class.java, JOB_ID, intent)
        }
    }


    override fun onCreate()
    {
        super.onCreate()
        running = true
        //Recupero del saldo dal Database
        myDB = MyOpenHelperDB(this).writableDatabase
        val sqlQuery = "SELECT * FROM " + MyOpenHelperDB.TABLE_USER
        val res = myDB.rawQuery(sqlQuery, null)
        res.moveToFirst()
        saldo = res.getString(res.getColumnIndex(MyOpenHelperDB.SALDO)).toShort()
        res.close()
    }

    /********** esecuzione del "lavoro" che deve eseguire il service  **********/
    override fun onHandleWork(intent: Intent)
    {

        val start = intent.getLongExtra("start", SystemClock.elapsedRealtime()) //estrazione dell'extra passato : start time del cronometro
        val timer_not = makeGenNotification()
        //Mostro la notifica
        PGMNotificationManager.setBuilder(timer_not)
        //Dichiaro il service importante per il foreground
        startForeground(PGMCostant.GENERIC_CHANNEL.v, PGMNotificationManager.build())

        while (running)
        {
            delta = (SystemClock.elapsedRealtime() - start) //tempo trascorso dall'avvio in ms
            //aggiornamento della notifica
            updateGenNotification(getTimerFormat(delta / 1000), timer_not as GenericNotificationBuilder)

            //prelievo dal saldo e invio del saldo attuale per aggiornare la view di Navigation Activity
            if(delta /1000 != 0L && delta /1000 % RIF == 0L) //"pull" euro ogni "RIF" secondi
            {
                val current = takeAmount() //saldo corrente : viene prelevata la quantità "pull" ogni "RIF" secondi

                //invio dell'update del saldo: verrà poi gestito da Nvigation Activity per l'aggiornamento della view
                val i = Intent("UPDATE")
                i.putExtra("saldo", "$current")
                sendBroadcast(i)
            }

            try {
                Thread.sleep(1000) //pausa del thread corrente per 1 secondo
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
        deleteNotification() //la notifica del conteggio viene cancellata automaticamente
    }

    override fun onDestroy()
    {
        stopForeground(true)  //il service ora non è più importante...
        super.onDestroy()
    }

    /********** metodo per l'aggiornamento della notifica **********/
    private fun updateGenNotification(elaps_time: String, notification: GenericNotificationBuilder) {
        notification.setBigText("${getString(R.string.ElapsedTime)} : $elaps_time")
        PGMNotificationManager.setBuilder(notification)
        PGMNotificationManager.show(this)
    }

    /********** metodo per creazione e il lancio di una  notifica generale **********/
    private fun makeGenNotification(): NotificationBuilder
    {
        //recupero della referenza della notifica generale
        var timNot : GenericNotificationBuilder
        timNot = try
        {
            val timNullable = PGMNotificationManager.getRef(PGMCostant.ID_GEN_NOT.v) as GenericNotificationBuilder?
            if(timNullable!=null) timNullable
            else throw NullPointerException()
        }
        catch (e: NullPointerException)
        {
            //Se non è presente la notifica nel registro delle notifiche la si crea da zero
            buildGenNot()
        }

        //Creazione della notifica generica
        //L'intent associato lancia activity1 perchè è lei a controllare se NavigationActivity è attiva
        val myIntent = Intent(this, Activity1::class.java)
                .setAction(Intent.ACTION_VIEW)
                .apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP }


        timNot.apply {
            setTitleandText(getString(R.string.TimerNotTitle), getString(R.string.TimerNotContent))
            setIntent(myIntent)
            setTimerStyle(true)
        }

        //costruzione e invio della notifica
        PGMNotificationManager.setBuilder(timNot)
        PGMNotificationManager.show(this)
        return timNot
    }

    /********** metodo per eliminazione della notifica generica **********/
    private fun deleteNotification()
    {
        val myNotificationManager = PGMNotificationManager.getNotificationManager(this)
        myNotificationManager.cancel(PGMCostant.GENERIC_CHANNEL.v) //rimozione notifiche generiche
    }

    /********** metodo per la conversione dei secondi passati in formato HH:MM:SS **********/
    private fun getTimerFormat(delta: Long): String
    {
        val seconds: Byte //max 59 s
        var minutes: Byte //max 59 min
        val hours: Int
        var format = ""

        if (delta < 60) {
            seconds = delta.toByte()
            format = "00:"
        } else {
            minutes = (delta / 60).toByte()
            if (minutes < 60) {
                seconds = (delta % 60).toByte()
                format = "$minutes:"
            } else {
                hours = (minutes / 60)
                minutes = (minutes % 60).toByte()
                seconds = (delta % 60).toByte()
                format = "$hours:$minutes:"
            }
        }

        format += if (seconds > 9) "$seconds" else "0$seconds"
        return format
    }

    /********** scalo della quantità di default dal saldo corrente **********/
    private fun takeAmount() : Short
    {
        /*Se il saldo corrente è minore di pull, l'utente non ha abbastanza soldi per poter
        * proseguire il viaggio*/
        if(saldo >= pull)
        {
            saldo = (saldo - pull).toShort()
            updateWallet(saldo.toString()) //aggiornamento del saldo in database
        }
        /*si verifica subito che il saldo non sia sceso sotto la quantità minima altrimenti si ferma
          il viaggio e si lancia un messaggio di avviso*/
        if(saldo < pull)
        {
            makeMsgNotification()
            running = false //stop del timer
        }
        return saldo
    }


    /********** aggiornamento del portafoglio direttamente dal database**********/
    private fun updateWallet(saldo: String)
    {
        val tmp = ContentValues()
        tmp.put(MyOpenHelperDB.SALDO, saldo)
        myDB.update(MyOpenHelperDB.TABLE_USER, tmp, null, null) //update
    }

    /********** metodo per la creazione e il lancio di una notifica di messaggistica **********/
    private fun makeMsgNotification()
    {
        //recupero della referenza della notifica di messaggio
        var botNot:MessageNotificationBuilder
        botNot = try
        {
            val mNNullable = PGMNotificationManager.getRef(PGMCostant.ID_MSG_NOT.v) as MessageNotificationBuilder?
            if(mNNullable!=null) mNNullable
            else throw NullPointerException()
        }
        catch (e: NullPointerException)
        {
            //Se non è presente la notifica nel registro delle notifiche la si crea da zero
            buildMsgNot()
        }

        //Creazione della notifica di messaggistica
        //L'intent associato lancia activity1 perchè è lei a controllare se NavigationActivity è attiva
        val myIntent = Intent(this, BubbleActivity::class.java)
                .setAction(Intent.ACTION_VIEW)
                .apply { flags =  Intent.FLAG_ACTIVITY_CLEAR_TASK }

        //impostazione intent e messaggio di notifica
        botNot.setIntent(myIntent)
        botNot.setTitleandText(getString(R.string.assistant_name), getString(R.string.balanceAlert))

        //creazione del messaggio
        val id = 2 * Chat.k
        Chat.k = id
        val savingsOut = MyMessage(id, getString(R.string.balanceAlert), System.currentTimeMillis())
        Chat.addMessage(savingsOut) //aggiunta del messaggio nella lista della chat

        if(BubbleActivity.called) BubbleActivity.updateAdapter() //update "live" della chat se attiva
        //costruzione della notifica
        PGMNotificationManager.setBuilder(botNot)
        PGMNotificationManager.show(this)
    }

    /********** costruzione notifica generica (se riferimento non presente)  **********/
    private fun buildGenNot():GenericNotificationBuilder
    {
        //NOTIFICA GENERALE
        val gN= GenericNotificationBuilder(
                this,
                PGMCostant.GENERIC_CHANNEL.v,
                getString(R.string.GENERIC_CH_NAME),
                getString(R.string.GENERIC_CH_DESC)
        )

        val myIntent = Intent(this, Activity1::class.java)
                .setAction(Intent.ACTION_VIEW)
                .apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TASK }

        gN.setIntent(myIntent)
        PGMNotificationManager.register(PGMCostant.ID_GEN_NOT.v,gN) //Registro la referenza di quel costruttore

        return gN
    }

    /********** costruzione notifica messaggistica (se riferimento non presente)  **********/
    private fun buildMsgNot():MessageNotificationBuilder
    {
        //NOTIFICA MESSAGGISTICA
        val mN= MessageNotificationBuilder(
                this,
                PGMCostant.MESG_CHANNEL.v,
                getString(R.string.MSG_CH_NAME),
                getString(R.string.MSG_CH_DESC)
        )
        val myIntent = Intent(this, BubbleActivity::class.java)
                .setAction(Intent.ACTION_VIEW)
                .apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TASK }
        mN.setIntent(myIntent)
        PGMNotificationManager.register(PGMCostant.ID_MSG_NOT.v,mN) //Registro la referenza di quel costruttore

        return mN
    }
}